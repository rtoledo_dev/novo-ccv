<?php
App::uses('AppModel', 'Model');
/**
 * Distributor Model
 *
 * @property Manufacturer $Manufacturer
 */
class Distributor extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'manufacturer_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'fiscal_name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cnpj' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'state_reference' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'company_type' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Manufacturer' => array(
			'className' => 'Manufacturer',
			'foreignKey' => 'manufacturer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);

/**
 * hasOne associations
 *
 * @var array
 */
	public $hasOne = array(
		'DistributorInformation' => array(
			'className' => 'DistributorInformation',
			'foreignKey' => 'company_id',
		),
		'CompanyContact' => array(
			'className' => 'CompanyContact',
			'foreignKey' => 'company_id',
		),
		'CompanyConfiguration' => array(
			'className' => 'CompanyConfiguration',
			'foreignKey' => 'company_id',
		),
		'CompanyAddress' => array(
			'className' => 'CompanyAddress',
			'foreignKey' => 'company_id',
		),
	);


	public function afterSave($options)
	{
		if(empty($this->data['CompanyConfiguration']['group_id'])){
			$this->CompanyConfiguration->Group->GroupCompany->deleteAll(array('GroupCompany.company_id' => $this->id));
		}else{
			$this->CompanyConfiguration->Group->GroupCompany->updateAll(
				array('GroupCompany.group_id' => $this->data['CompanyConfiguration']['group_id']),
				array('GroupCompany.company_id' => $this->id)
			);
		}
		// debug($this->data);
		// die;
		return true;
	}
}
