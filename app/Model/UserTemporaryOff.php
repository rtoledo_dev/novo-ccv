<?php
App::uses('AppModel', 'Model');
/**
 * UserTemporaryOff Model
 *
 * @property User $User
 * @property UserTemporaryOffMotive $UserTemporaryOffMotive
 */
class UserTemporaryOff extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'user_temporary_off_motive_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		// 'document' => array(
		// 	'notempty' => array(
		// 		'rule' => array('notempty'),
		// 		'message' => '* Deve ser preenchido',
		// 		//'allowEmpty' => false,
		// 		//'required' => false,
		// 		//'last' => false, // Stop validation after this rule
		// 		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		// 	),
		// ),
		'start_at' => array(
			'date' => array(
				'rule' => array('date'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'end_at' => array(
			'date' => array(
				'rule' => array('date'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'UserTemporaryOffMotive' => array(
			'className' => 'UserTemporaryOffMotive',
			'foreignKey' => 'user_temporary_off_motive_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $actsAs = array(
	'Upload.Upload' => array(
		'document'
		)
	);

	public function beforeValidate($options = array()) {
    $this->dateFormatBeforeSave($this->data[$this->name]['start_at']);
    $this->dateFormatBeforeSave($this->data[$this->name]['end_at']);
	  return true;
	}
}
