<?php
App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class SearchHelper extends Helper {
  public function isSelected($form,$field,$value)
  {
    return $form && @$form[$field] == $value ? 'selected="selected"' : '';
  }
}
