<?php
App::uses('AdminController', 'Controller');
/**
 * Aovs Controller
 *
 * @property Aov $Aov
 */
class AovsController extends AdminController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Aov->recursive = 0;
		$this->Aov->order = 'Aov.name';
		$conditions = $this->_buildTableSearchConditions(array('Aov.name LIKE', 'UserEmployee.name LIKE'),@$this->request->query['q']);
		$this->set('aovs', $this->paginate('Aov', $conditions));
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Aov->id = $id;
		if (!$this->Aov->exists()) {
			throw new NotFoundException(__('Invalid %s', __('aov')));
		}
		$this->set('aov', $this->Aov->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Aov->create();
			if ($this->Aov->save($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('aov')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('aov')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$userEmployees = $this->Aov->UserEmployee->find('list', array('order' => 'name'));
		$this->set(compact('userEmployees'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Aov->id = $id;
		if (!$this->Aov->exists()) {
			throw new NotFoundException(__('Invalid %s', __('aov')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Aov->save($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('aov')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('aov')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Aov->read(null, $id);
		}
		$userEmployees = $this->Aov->UserEmployee->find('list', array('order' => 'name'));
		$this->set(compact('userEmployees'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Aov->id = $id;
		if (!$this->Aov->exists()) {
			throw new NotFoundException(__('Invalid %s', __('aov')));
		}
		if ($this->Aov->delete()) {
			$this->Session->setFlash(
				__('A informação foi removida com sucesso.', __('aov')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(
			__('A informação não pode ser removida. Existe uma dependência da mesma no sistema.', __('aov')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect($this->referer());
	}
}
