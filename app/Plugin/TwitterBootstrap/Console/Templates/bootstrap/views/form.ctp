<?php echo "<?php echo \$this->Html->breadcrumb(array('<a href=\"#\">Cadastros</a>', \$this->Html->link('Lista de {$modelClass}s', array('action' => 'index')), 'Cadastro {$modelClass}')); ?>" ?>

<h3 class="heading">Cadastro <?php echo $modelClass ?></h3>

<div class="row-fluid">
	<div class="span12">
		<?php echo "<?php echo \$this->BootstrapForm->create('{$modelClass}', array('class' => 'form-horizontal well'));?>\n";?>
			<?php echo "<?php echo \$this->element('Forms/{$modelClass}') ?>\n" ?>
			<fieldset>
				<legend class="block-title"><?php echo "<?php echo __('Dados %s', __('" . $singularHumanName . "')); ?>"; ?></legend>
<?php
				echo "\t\t\t\t<?php\n";
				$id = null;
				foreach ($fields as $field) {
					if (strpos($action, 'add') !== false && $field == $primaryKey) {
						continue;
					} elseif (!in_array($field, array('created', 'modified', 'updated'))) {
						if ($field == $primaryKey) {
							$id = "\t\t\t\techo \$this->BootstrapForm->hidden('{$field}');\n";
						} else {
							// if($this->templateVars['schema'][$field]['null'] == false){
							// 	$required = ", array(\n\t\t\t\t\t'required' => 'required',\n\t\t\t\t\t'helpInline' => '<span class=\"label label-important\">' . __('Obrigatório') . '</span>&nbsp;')\n\t\t\t\t";
							// } else {
							// 	$required = null;
							// }
							$required = null;
							echo "\t\t\t\techo '<div class=\"row-fluid\">';\n";
							echo "\t\t\t\t\techo '<div class=\"span6\">';\n";
							echo "\t\t\t\t\techo \$this->BootstrapForm->input('{$field}'{$required});\n";
							echo "\t\t\t\t\techo '</div>';\n";
							echo "\t\t\t\techo '</div>';\n";
						}
					}
				}
				echo $id;
				unset($id);
				if (!empty($associations['hasAndBelongsToMany'])) {
					foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
						echo "\t\t\t\techo \$this->BootstrapForm->input('{$assocName}');\n";
					}
				}
				echo "\t\t\t\t?>\n";
				echo "\t\t\t\t<?php echo \$this->BootstrapForm->submit(__('Salvar'));?>\n";
?>
			</fieldset>
		<?php
			echo "<?php echo \$this->BootstrapForm->end();?>\n";
		?>
	</div>
</div>
