<?php
App::uses('AdminController', 'Controller');
/**
 * Companies Controller
 *
 * @property UserEmployee $UserEmployee
 */
class UserEmployeesController extends AdminController {
	var $uses = array('UserEmployee','SearchUserEmployee', 'FullRole');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SearchUserEmployee->recursive = 0;
		$this->SearchUserEmployee->order = 'SearchUserEmployee.name';
		$conditions = $this->_buildTableSearchConditions(array('SearchUserEmployee.name LIKE',
			'UserContact.birthday',
			'UserContact.cpf LIKE',
			'Company.name LIKE',
			'SearchUserEmployee.admitted_at',
			'ProfessionalPosition.name LIKE',
			'Role.name LIKE',
			'UserConfiguration.status LIKE',
			),@$this->request->query['q'], @$this->request->query['column']);
		$this->set('userEmployees', $this->paginate('SearchUserEmployee', $conditions));
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->UserEmployee->id = $id;
		if (!$this->UserEmployee->exists()) {
			throw new NotFoundException(__('Invalid %s', __('userEmployee')));
		}
		$this->set('userEmployee', $this->UserEmployee->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->UserEmployee->create();
			if ($this->UserEmployee->saveAll($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('userEmployee')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('userEmployee')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$this->_buildFormAssociations();
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->UserEmployee->id = $id;
		if (!$this->UserEmployee->exists()) {
			throw new NotFoundException(__('Invalid %s', __('userEmployee')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			// debug($this->request->data);die;
			if ($this->UserEmployee->saveAll($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('userEmployee')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('userEmployee')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->UserEmployee->read(null, $id);

		}
		$this->_buildFormAssociations();
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->UserEmployee->id = $id;
		if (!$this->UserEmployee->exists()) {
			throw new NotFoundException(__('Invalid %s', __('userEmployee')));
		}
		if ($this->UserEmployee->delete()) {
			$this->Session->setFlash(
				__('A informação foi removida com sucesso.', __('userEmployee')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(
			__('A informação não pode ser removida. Existe uma dependência da mesma no sistema.', __('userEmployee')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect($this->referer());
	}

	public function _buildFormAssociations()
	{
		$states = $this->UserEmployee->UserAddress->State->find('list');
		$cities = array();
		if(!empty($this->request->data['UserAddress']['state_id'])){
			$cities = $this->UserEmployee->UserAddress->City->find('list', array('conditions' => array(
				'City.state_id' => $this->request->data['UserAddress']['state_id']
				)
			));
		}

		$branchs = $this->UserEmployee->Branch->find('list');
		$roles = $this->FullRole->find('list', array('conditions' => array('FullRole.user_type' => 'Funcionário')));
		$tshirts = $this->UserEmployee->UserGarb->Tshirt->find('list');
		$shoes = $this->UserEmployee->UserGarb->Shoe->find('list');
		$educationFormationGroups = $this->UserEmployee->EducationFormation->EducationFormationGroup->find('all');
		$manufacturers = $this->UserEmployee->Manufacturer->find('list');
		$professionalPositions = $this->UserEmployee->UserProfessionalInformation->ProfessionalPosition->find('list');
		$professionalDepartments = $this->UserEmployee->UserProfessionalInformation->ProfessionalDepartment->find('list');
		$userInactiveReasons = $this->UserEmployee->UserConfiguration->UserInactiveReason->find('list');
		$userInactiveMotives = $this->UserEmployee->UserConfiguration->UserInactiveMotive->find('list');
		$this->set(compact('branchs', 'roles', 'states', 'cities', 'tshirts',
			'shoes', 'educationFormationGroups', 'manufacturers', 'professionalPositions',
			'userInactiveReasons', 'userInactiveMotives',
			'professionalDepartments'));
	}

}
