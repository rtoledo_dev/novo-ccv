<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
  public function dateFormatBeforeSave(&$dateString) {
    if(empty($dateString) || strpos($dateString, "/") === false){
      return $dateString;
    }
    $dateString = join('-',array_reverse(explode('/',$dateString)));
    return $dateString;
  }

  /**
  * Validations functions
  */

  public function equalToEmail($check)
  {
    if(empty($this->data[$this->name]['email']) && isset($this->id)){
        return true;
    }
    if(strcmp($this->data[$this->name]['email'],$this->data[$this->name]['confirm_email']) ==0 ){
      return true;
    }
    return false;
  }



  /**
 * Checks SSN for Brazil
 *
 * @param string $check The value to check.
 * @return boolean
 */
  public static function ssn($check) {
    return AppModel::cpf($check) || AppModel::cnpj($check);
  }

/**
 * Checks CPF for Brazil
 *
 * @param string $check The value to check.
 * @return boolean
 */
  public function cpf($check) {
    $check = trim(@$this->data[$this->name]['cpf']);

    // sometimes the user submits a masked CNPJ
    if (preg_match('/^\d\d\d.\d\d\d.\d\d\d\-\d\d/', $check)) {
      $check = str_replace(array('-', '.', '/'), '', $check);
    } elseif (!ctype_digit($check)) {
      return false;
    }

    

    if (strlen($check) != 11) {
      return false;
    }

    // repeated values are invalid, but algorithms fails to check it
    for ($i = 0; $i < 10; $i++) {
      if (str_repeat($i, 11) === $check) {
        return false;
      }
    }

    $dv = substr($check, -2);
    for ($pos = 9; $pos <= 10; $pos++) {
      $sum = 0;
      $position = $pos + 1;
      for ($i = 0; $i <= $pos - 1; $i++, $position--) {
        $sum += $check[$i] * $position;
      }
      $div = $sum % 11;
      if ($div < 2) {
        $check[$pos] = 0;
      } else {
        $check[$pos] = 11 - $div;
      }
    }
    $dvRight = $check[9] * 10 + $check[10];

    return ($dvRight == $dv);
  }

/**
 * Checks CNPJ for Brazil
 *
 * @param string $check The value to check.
 * @return boolean
 */
  public static function cnpj($check) {
    $check = trim($check);
    // sometimes the user submits a masked CNPJ
    if (preg_match('/^\d\d.\d\d\d.\d\d\d\/\d\d\d\d\-\d\d/', $check)) {
      $check = str_replace(array('-', '.', '/'), '', $check);
    } elseif (!ctype_digit($check)) {
      return false;
    }

    if (strlen($check) != 14) {
      return false;
    }
    $firstSum = ($check[0] * 5) + ($check[1] * 4) + ($check[2] * 3) + ($check[3] * 2) +
      ($check[4] * 9) + ($check[5] * 8) + ($check[6] * 7) + ($check[7] * 6) +
      ($check[8] * 5) + ($check[9] * 4) + ($check[10] * 3) + ($check[11] * 2);

    $firstVerificationDigit = ($firstSum % 11) < 2 ? 0 : 11 - ($firstSum % 11);

    $secondSum = ($check[0] * 6) + ($check[1] * 5) + ($check[2] * 4) + ($check[3] * 3) +
      ($check[4] * 2) + ($check[5] * 9) + ($check[6] * 8) + ($check[7] * 7) +
      ($check[8] * 6) + ($check[9] * 5) + ($check[10] * 4) + ($check[11] * 3) +
      ($check[12] * 2);

    $secondVerificationDigit = ($secondSum % 11) < 2 ? 0 : 11 - ($secondSum % 11);

    return ($check[12] == $firstVerificationDigit) && ($check[13] == $secondVerificationDigit);
  }
}
