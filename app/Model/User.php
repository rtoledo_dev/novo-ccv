<?php
App::uses('AppModel', 'Model');
App::uses('AuthComponent', 'Controller/Component');
/**
 * User Model
 *
 * @property Area $Area
 * @property Base $Base
 */
class User extends AppModel {

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Preenchimento obrigatório.',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'email' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Preenchimento obrigatório.',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'email' => array(
                'rule' => array('email'),
                'message' => 'Deve ser um email válido.',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'Já existe um registro com esse valor',
                'allowEmpty' => true,
            ),
        ),
        'pass' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Preenchimento obrigatório.',
                'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'minLength' => array(
                'rule' => array('minLength',3),
                'message' => 'Mínimo de 3 caracteres.',
                'allowEmpty' => true,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'confirm_password' => array(
            'equalToPassword' => array(
                'rule' => array('equalToPassword'),
                'message' => 'Não confere com a senha.',
                //'allowEmpty' => false,
                'required' => false,
                //'last' => false, // Stop validation after this rule
                // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'personal_email' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Preenchimento obrigatório.',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'email' => array(
                'rule' => array('email'),
                'message' => 'Deve ser um email válido.',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        )
    );

    /**
     * Components and extensions
     */
    var $actsAs = array(
        'Upload.Upload' => array(
            'photo' => array(
                'fields' => array(
                    'dir' => 'photo_dir'
                ),
                'thumbsizes' => array(
                    '80x60' => '80x60',
                    '201x151' => '201x151',
                    '800x600' => '800x600',
                ),
                'thumbnailMethod'   => 'php',
            )
        )
    );

    /**
     * Callbacks
     */
    function beforeSave($options = array()) {
        if (isset($this->data[$this->name]['pass']) && !empty($this->data[$this->name]['pass'])){
            $this->data[$this->name]['pass'] = AuthComponent::password($this->data[$this->name]['pass']);
        }else{
            unset($this->data[$this->name]['pass']);
        }
        return true;
    }


    /**
     * Validations functions
     */

    public function equalToPassword($check)
    {
        if(empty($this->data['User']['pass']) && isset($this->id)){
            return true;
        }
        if(strcmp($this->data['User']['pass'],$this->data['User']['confirm_password']) ==0 ){
          return true;
        }
        return false;
    }
}
