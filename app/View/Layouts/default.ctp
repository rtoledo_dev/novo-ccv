<!DOCTYPE html>
<html lang="en">
   <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Portal Toyota CCV/TSW</title>
    <?php
    echo $this->Html->css(array(
    '/bootstrap/css/bootstrap.min',
    '/bootstrap/css/bootstrap-responsive.min',
    'dark',
    '/lib/jBreadcrumbs/css/BreadCrumb',
    '/lib/qtip2/jquery.qtip.min',
    '/lib/google-code-prettify/prettify',
    '/lib/sticky/sticky',
    '/img/splashy/splashy',
    '/lib/multiselect/css/multi-select',
    '/lib/colorbox/colorbox',
    '/lib/chosen/chosen',
    'jquery.datepicker',
    'jquery-ui',
    '/lib/datepicker/datepicker',
    'style',
    'ccv'
    ));
    ?>

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="/favicon.ico" />

    <!--[if lte IE 8]>
    <?php echo $this->Html->css(array('ie')) ?>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <?php echo $this->Html->script(array('/lib/flot/excanvas.min')); ?>
    <![endif]-->
    <script>
      document.getElementsByTagName('html')[0].className = 'js';
    </script>
  </head>
  <body>
    <div id="maincontainer" class="clearfix">
      <!-- header -->
      <header>
        <div class="navbar navbar-fixed-top">
          <div class="navbar-inner">
            <div class="container-fluid">
              <a class="brand" href="<?php echo $this->Html->url(array('controller' => 'RealEstates', 'action' => 'index', 'type' => 'Aluguel')) ?>"><i class="icon-home icon-white"></i> Portal Toyota CCV/TSW</a>
              <ul class="nav user_menu pull-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->Session->read('Auth.User.name') ?> <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo $this->Html->url(array('controller' => 'Users', 'action' => 'edit', $this->Session->read('Auth.User.id'))) ?>">Meus dados</a></li>
                    <li><a href="<?php echo $this->Html->url(array('controller' => 'Users', 'action' => 'changePass')) ?>">Alterar senha</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo $this->Html->url(array('controller' => 'Login', 'action' => 'logout')) ?>">SAIR</a></li>
                  </ul>
                </li>
              </ul>
              <a data-target=".nav-collapse" data-toggle="collapse" class="btn_menu">
                <span class="icon-align-justify icon-white"></span>
              </a>
              <nav>
                <div class="nav-collapse">
                  <?php echo $this->element('Layout/menu') ?>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </header>
            
      <!-- main content -->
      <div id="contentwrapper">
        <div class="main_content">
          <?php echo $this->Session->flash(); ?>
          <?php echo $this->fetch('content'); ?>
        </div>
      </div>
      <?php echo $this->Html->script(array( 
      'jquery.min',
      'jquery-migrate.min',
      'jquery.debouncedresize.min',
      'jquery.cookie.min',
      '/bootstrap/js/bootstrap.min',
      '/lib/google-code-prettify/prettify.min',
      '/lib/qtip2/jquery.qtip.min',
      '/lib/jBreadcrumbs/js/jquery.jBreadCrumb.1.1.min',
      '/lib/jquery-ui/jquery-ui-1.8.20.custom.min',
      'forms/jquery.ui.touch-punch.min',
      'forms/jquery.inputmask.min',
      'forms/jquery.autosize.min',
      'forms/jquery.counter.min',
      '/lib/datepicker/bootstrap-datepicker',
      '/lib/datepicker/locales/bootstrap-datepicker.br',
      '/lib/datepicker/bootstrap-timepicker.min',
      '/lib/tag_handler/jquery.taghandler.min',
      'forms/jquery.spinners.min',
      '/lib/uniform/jquery.uniform.min',
      'forms/jquery.progressbar.anim',
      '/lib/multiselect/js/jquery.multi-select.min',
      '/lib/chosen/chosen.jquery',
      '/lib/tiny_mce/jquery.tinymce',
      'selectNav',
      'gebo_common',
      'gebo_forms',
      'jquery.imagesloaded.min',
      'jquery.wookmark'
      ));

      echo $this->fetch('footerScripts');
      ?>
      <script type="text/javascript">
        $(document).ready(function() {
          gebo_sidebar.make();
          setTimeout('$("html").removeClass("js")',1000);
        });
      </script>
    </div>
    <?php echo $this->element('sql_dump'); ?>
  </body>
</html> 
  
