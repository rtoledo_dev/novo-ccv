<?php echo $this->Html->breadcrumb(array('<a href="#">Cadastros</a>', $this->Html->link('Lista de Regiões', array('action' => 'index')), 'Cadastro Região')); ?>
<h3 class="heading">Cadastro Região</h3>

<div class="row-fluid">
	<div class="span12">
		<?php echo $this->BootstrapForm->create('Region', array('class' => 'form-vertical well'));?>
			<?php echo $this->element('Forms/region') ?>
			<?php echo $this->BootstrapForm->hidden('id'); ?>
		<?php echo $this->BootstrapForm->end();?>
	</div>
</div>
