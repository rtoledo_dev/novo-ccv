<fieldset>
  <legend class="block-title"><?php echo __('Dados Cadastrais'); ?></legend>
  <?php
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('manufacturer_id', array('label' => 'Marca', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('DistributorInformation.dlr_toyos', array('label' => 'DLR Toyos'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('name', array('label' => 'Nome Fantasia'));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span12">';
    echo $this->BootstrapForm->input('fiscal_name', array('label' => 'Razão Social', 'size' => 80));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('cnpj', array('label' => 'CNPJ', 'class' => 'mask-cnpj'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('domain', array('label' => 'Domínio', 'size' => 50));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('state_reference', array('label' => 'Inscrição Estadual', 'size' => 20));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('DistributorInformation.initialized_at', array(
      'label' => 'Data de Início da Operação', 'type' => 'text',
      'value' => $this->SimpleForm->formatDateFromModel(@$this->request->data['DistributorInformation']['initialized_at'])));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Endereço'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyAddress.zipcode', array('label' => 'CEP',
      'class' => 'mask-zipcode',
      'data-zipcode-url' => $this->Html->url(array('action' => 'addressByZipcode'))));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyAddress.state_id', array('label' => 'UF',
      'data-cities-url' => $this->Html->url(array('action' => 'citiesByState')),
      'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyAddress.city_id', array('label' => 'Cidade', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyAddress.address', array('label' => 'Endereço'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyAddress.address_complement', array('label' => 'Complemento', 'size' => 20));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Dados de Contato'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyContact.contact', array('label' => 'Contato'));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyContact.email', array('label' => 'Email'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyContact.email_confirmation', array('label' => 'Confirmação do Email'));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyContact.phone_comercial1', array('label' => 'Telefone Comercial 1', 'class' => 'mask-phone', 'size' => 10));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyContact.phone_comercial2', array('label' => 'Telefone Comercial 2', 'class' => 'mask-phone', 'size' => 10));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Informações'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span12">';
    echo $this->BootstrapForm->input('DistributorInformation.have_tv', array('type' => 'checkbox',
      'label' => 'Há TV ou Projetor'));
    echo $this->BootstrapForm->input('DistributorInformation.have_rest_room', array('type' => 'checkbox',
      'label' => 'Há banda larga'));
    echo $this->BootstrapForm->input('DistributorInformation.have_internet_high_speed', array('type' => 'checkbox',
      'label' => 'Há sala para acomodar sentada a equipe de vendas'));
    echo $this->BootstrapForm->input('DistributorInformation.have_personal_computer', array('type' => 'checkbox',
      'label' => 'Há computador de uso individual para cada Consultor de Vendas acessar o Portal CCV'));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Configurações'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4" id="companyConfigurationAov">';
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyConfiguration.region_id', array('label' => 'Região', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyConfiguration.group_id', array('label' => 'Grupo', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyConfiguration.certification_tsw_id', array('label' => 'Certificação TSW', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyConfiguration.company_rating_id', array('label' => 'Classificação', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyConfiguration.status', array('label' => 'Status', 'options' => $this->SimpleForm->distributorsStatusOptions()));
    echo '</div>';
  echo '</div>';
  ?>
  <?php echo $this->BootstrapForm->submit(__('Salvar'));?>
</fieldset>

<?php $this->start('footerScripts') ?>
<script type="text/javascript">
$(function(){
  $('#DistributorInformationInitializedAt').datepicker({format: "dd/mm/yyyy", language: 'br'})

  updateCompanyConfigurationAov();
  $("select#CompanyConfigurationRegionId").change(function(){
    updateCompanyConfigurationAov()
  })
})
function updateCompanyConfigurationAov(){
  var id = $("select#CompanyConfigurationRegionId").val();
  $.get('<?php echo $this->Html->url(array('action' => 'updateCompanyConfigurationAov')) ?>',
    {region_id: id},
    function(data){
      $('#companyConfigurationAov').html(data);
    }
  )
}
</script>
<?php $this->end() ?>
