<?php
App::uses('AppModel', 'Model');
/**
 * SearchRegion Model
 *
 * @property Aov $Aov
 */
class SearchRegion extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Aov' => array(
			'className' => 'Aov',
			'foreignKey' => 'aov_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
