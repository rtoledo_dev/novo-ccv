<?php
App::uses('AdminController', 'Controller');
/**
 * Companies Controller
 *
 * @property UserDistributor $UserDistributor
 */
class UserDistributorsController extends AdminController {
	var $uses = array('UserDistributor','SearchUserDistributor', 'FullRole');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SearchUserDistributor->recursive = 0;
		$this->SearchUserDistributor->order = 'SearchUserDistributor.name';
		$conditions = $this->_buildTableSearchConditions(array('SearchUserDistributor.name LIKE',
			'UserContact.birthday',
			'UserContact.cpf LIKE',
			'Company.name LIKE',
			'ProfessionalOccupation.name LIKE',
			'ProfessionalPosition.name LIKE',
			'Role.name LIKE',
			'UserConfiguration.see_group LIKE',
			'UserConfiguration.status LIKE',
			),@$this->request->query['q'], @$this->request->query['column']);
		$this->set('userDistributors', $this->paginate('SearchUserDistributor', $conditions));
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->UserDistributor->id = $id;
		if (!$this->UserDistributor->exists()) {
			throw new NotFoundException(__('Invalid %s', __('userDistributor')));
		}
		$this->set('userDistributor', $this->UserDistributor->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->UserDistributor->create();
			if ($this->UserDistributor->saveAll($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('userDistributor')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('userDistributor')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$this->_buildFormAssociations();
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->UserDistributor->recursive = 2;
		$this->UserDistributor->id = $id;
		if (!$this->UserDistributor->exists()) {
			throw new NotFoundException(__('Invalid %s', __('userDistributor')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			// debug($this->request->data);die;
			if ($this->UserDistributor->saveAll($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('userDistributor')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$validationErrors = $this->UserDistributor->validationErrors;
				$this->set('validationErrors', $validationErrors);
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('userDistributor')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->UserDistributor->read(null, $id);
		}
		$this->_buildFormAssociations();
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->UserDistributor->id = $id;
		if (!$this->UserDistributor->exists()) {
			throw new NotFoundException(__('Invalid %s', __('userDistributor')));
		}
		if ($this->UserDistributor->delete()) {
			$this->Session->setFlash(
				__('A informação foi removida com sucesso.', __('userDistributor')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(
			__('A informação não pode ser removida. Existe uma dependência da mesma no sistema.', __('userDistributor')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect($this->referer());
	}

	public function _buildFormAssociations()
	{
		$states = $this->UserDistributor->UserAddress->State->find('list');
		$cities = array();
		if(!empty($this->request->data['UserAddress']['state_id'])){
			$cities = $this->UserDistributor->UserAddress->City->find('list', array('conditions' => array(
				'City.state_id' => $this->request->data['UserAddress']['state_id']
				)
			));
		}

		$distributors = $this->UserDistributor->Distributor->find('list');
		$roles = $this->FullRole->find('list', array('conditions' =>
			array('FullRole.user_type' => 'Distribuidor')));
		$tshirts = $this->UserDistributor->UserGarb->Tshirt->find('list');
		$shoes = $this->UserDistributor->UserGarb->Shoe->find('list');
		$educationFormationGroups = $this->UserDistributor->EducationFormation->EducationFormationGroup->find('all');
		$manufacturers = $this->UserDistributor->Manufacturer->find('list');
		$professionalDepartments = $this->UserDistributor->ProfessionalOtherDepartment->find('list');
		$professionalPositions = $this->UserDistributor->UserProfessionalInformation->ProfessionalPosition->find('list');
		$professionalOccupations = $this->UserDistributor->UserProfessionalInformation->ProfessionalOccupation->find('list');
		$userInactiveReasons = $this->UserDistributor->UserConfiguration->UserInactiveReason->find('list');
		$userInactiveMotives = $this->UserDistributor->UserConfiguration->UserInactiveMotive->find('list');
		$userTemporaryOffMotives = $this->UserDistributor->UserTemporaryOff->UserTemporaryOffMotive->find('list');
		$this->set(compact('distributors', 'roles', 'states', 'cities', 'tshirts',
			'shoes', 'educationFormationGroups', 'manufacturers',
			'userInactiveMotives', 'userInactiveReasons', 'userTemporaryOffMotives',
			'professionalDepartments', 'professionalPositions', 'professionalOccupations'));
	}

}
