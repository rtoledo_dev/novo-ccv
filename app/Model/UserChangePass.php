<?php
App::uses('AppModel', 'Model');
App::uses('AuthComponent', 'Controller/Component');
/**
 * User Model
 *
 */
class UserChangePass extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	
	public $useTable = 'users';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'current_pass' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Este campo es obligatorio.',
				'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'equaltToCurrent' => array(
				'rule' => array('equaltToCurrent'),
				'message' => 'No coincide con la contraseña atual.',
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				// 'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'pass' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Este campo es obligatorio.',
				'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
			),
			'minLength' => array(
				'rule' => array('minLength',3),
				'message' => 'Un mínimo de 3 caracteres.',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				// 'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'confirm_password' => array(
			'equalToPassword' => array(
				'rule' => array('equalToPassword'),
				'message' => 'No coincide con la contraseña.',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				// 'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		
	);

	/**
	 * Callbacks
	 */
	function beforeSave() {
		parent::beforeSave();
		if (isset($this->data[$this->name]['pass']) && !empty($this->data[$this->name]['pass'])){
			$this->data[$this->name]['pass'] = AuthComponent::password($this->data[$this->name]['pass']);
		}else{
			unset($this->data[$this->name]['pass']);
		}

		return true;
	}



	/**
	 * Validations functions
	 */

	public function equalToPassword($check)
	{
		if(empty($this->data['User']['pass']) && isset($this->id)){
			return true;
		}
		if(strcmp($this->data['User']['pass'],$this->data['User']['confirm_password']) ==0 ){
		  return true;
		}
		return false;
	}
	
	public function equaltToCurrent($check)
	{
	  $currentUser = $this->findById($this->id);
	  if($currentUser['UserChangePass']['pass'] != AuthComponent::password($this->data['UserChangePass']['current_pass'])){
	    return false;
	  }
	  
	  return true;
	}
}
