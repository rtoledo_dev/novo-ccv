<?php echo "<?php echo \$this->Html->breadcrumb(array('<a href=\"#\">Cadastros</a>', 'Lista de {$pluralHumanName}')); ?>\n" ?>
<h3 class="heading"><?php echo "<?php echo __('Lista de %s', __('{$pluralHumanName}'));?>";?></h3>

<div class="row-fluid">
	<div class="span4">
		<?php echo "<?php echo \$this->Html->link('<i class=\"icon-plus-sign\"></i> Cadastrar Novo',array('action' => 'add'), array('escape' => false, 'class' => 'btn')) ?>\n" ?>
	</div>
	<div class="span8">
		<?php echo "<?php echo \$this->Form->create(NULL,array('class' => 'form-search pull-right', 'type' => 'get')) ?>\n"?>
		<input type="text" placeholder="Buscar" name="q" value="<?php echo "<?php echo @\$this->request->query['q'] ?>"?>" />
		<?php echo "<?php echo \$this->Form->end(NULL) ?>\n"?>
	</div>
</div>

<div class="row-fluid search-table-row">
	<div class="span12">

		<table class="table table-striped table-bordered table-condensed">
			<thead>
			<tr>
<?php  foreach ($fields as $field):?>
				<th><?php echo "<?php echo \$this->BootstrapPaginator->sort('{$field}');?>";?></th>
<?php endforeach;?>
				<th class="td-options"></th>
			</tr>
			</thead>
			<tbody>
<?php
echo "\t\t<?php foreach (\${$pluralVar} as \${$singularVar}): ?>\n";
echo "\t\t\t<tr>\n";
	foreach ($fields as $field) {
		$isKey = false;
		if (!empty($associations['belongsTo'])) {
			foreach ($associations['belongsTo'] as $alias => $details) {
				if ($field === $details['foreignKey']) {
					$isKey = true;
					echo "\t\t\t\t<td>\n\t\t\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t\t\t</td>\n";
					break;
				}
			}
		}
		if ($isKey !== true) {
			echo "\t\t\t\t<td><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
		}
	}

	echo "\t\t\t\t<td class=\"actions\">\n";
 	echo "\t\t\t\t\t<?php echo \$this->Html->link('<i class=\"icon-edit\"></i>', array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class' => 'left-td-options', 'escape' => false, 'title' => __('Editar'))); ?>\n";
 	echo "\t\t\t\t\t<?php echo \$this->Form->postLink('<i class=\"icon-remove-sign align-icon\"></i>', array('action' => 'delete', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class'=> 'right-td-options', 'escape' => false, 'title' => __('Excluir')), __('Deseja prosseguir com esta ação?')); ?>\n";
	echo "\t\t\t\t</td>\n";
echo "\t\t\t</tr>\n";

echo "\t\t<?php endforeach; ?>\n";
?>
			</tbody>
		</table>
		<span class="pull-right">
			<?php echo "<?php echo \$this->BootstrapPaginator->pagination(); ?>\n"; ?>
		</span>

	</div>
</div>
