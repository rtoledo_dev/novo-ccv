<div class="row-fluid">
	<div class="span9">
		<h2><?php  echo __('Region');?></h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($region['Region']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('User'); ?></dt>
			<dd>
				<?php echo $this->Html->link($region['User']['name'], array('controller' => 'users', 'action' => 'view', $region['User']['id'])); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Aov'); ?></dt>
			<dd>
				<?php echo $this->Html->link($region['Aov']['name'], array('controller' => 'aovs', 'action' => 'view', $region['Aov']['id'])); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Name'); ?></dt>
			<dd>
				<?php echo h($region['Region']['name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Created At'); ?></dt>
			<dd>
				<?php echo h($region['Region']['created_at']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Updated At'); ?></dt>
			<dd>
				<?php echo h($region['Region']['updated_at']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Edit %s', __('Region')), array('action' => 'edit', $region['Region']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete %s', __('Region')), array('action' => 'delete', $region['Region']['id']), null, __('Are you sure you want to delete # %s?', $region['Region']['id'])); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Regions')), array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Region')), array('action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Users')), array('controller' => 'users', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('User')), array('controller' => 'users', 'action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Aovs')), array('controller' => 'aovs', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Aov')), array('controller' => 'aovs', 'action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Company Configurations')), array('controller' => 'company_configurations', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Company Configuration')), array('controller' => 'company_configurations', 'action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Region Companies')), array('controller' => 'region_companies', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Region Company')), array('controller' => 'region_companies', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span9">
		<h3><?php echo __('Related %s', __('Company Configurations')); ?></h3>
	<?php if (!empty($region['CompanyConfiguration'])):?>
		<table class="table">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Company Id'); ?></th>
				<th><?php echo __('Aov Id'); ?></th>
				<th><?php echo __('Region Id'); ?></th>
				<th><?php echo __('Group Id'); ?></th>
				<th><?php echo __('Created At'); ?></th>
				<th><?php echo __('Updated At'); ?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($region['CompanyConfiguration'] as $companyConfiguration): ?>
			<tr>
				<td><?php echo $companyConfiguration['id'];?></td>
				<td><?php echo $companyConfiguration['company_id'];?></td>
				<td><?php echo $companyConfiguration['aov_id'];?></td>
				<td><?php echo $companyConfiguration['region_id'];?></td>
				<td><?php echo $companyConfiguration['group_id'];?></td>
				<td><?php echo $companyConfiguration['created_at'];?></td>
				<td><?php echo $companyConfiguration['updated_at'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'company_configurations', 'action' => 'view', $companyConfiguration['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'company_configurations', 'action' => 'edit', $companyConfiguration['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'company_configurations', 'action' => 'delete', $companyConfiguration['id']), null, __('Are you sure you want to delete # %s?', $companyConfiguration['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>

	</div>
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('Company Configuration')), array('controller' => 'company_configurations', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="row-fluid">
	<div class="span9">
		<h3><?php echo __('Related %s', __('Region Companies')); ?></h3>
	<?php if (!empty($region['RegionCompany'])):?>
		<table class="table">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Region Id'); ?></th>
				<th><?php echo __('Company Id'); ?></th>
				<th><?php echo __('Created At'); ?></th>
				<th><?php echo __('Updated At'); ?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($region['RegionCompany'] as $regionCompany): ?>
			<tr>
				<td><?php echo $regionCompany['id'];?></td>
				<td><?php echo $regionCompany['region_id'];?></td>
				<td><?php echo $regionCompany['company_id'];?></td>
				<td><?php echo $regionCompany['created_at'];?></td>
				<td><?php echo $regionCompany['updated_at'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'region_companies', 'action' => 'view', $regionCompany['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'region_companies', 'action' => 'edit', $regionCompany['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'region_companies', 'action' => 'delete', $regionCompany['id']), null, __('Are you sure you want to delete # %s?', $regionCompany['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>

	</div>
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('Region Company')), array('controller' => 'region_companies', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
