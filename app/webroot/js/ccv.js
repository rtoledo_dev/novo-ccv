$(document).ready(function(){

  $('#CompanyAddressZipcode').change(function(){
    $.getJSON($(this).data('zipcode-url'),
      {zipcode: $(this).val()},
      function(data){
        if(data['success'] != 0){
          $('#CompanyAddressAddress').val(data['address'])
          if(data['state_id'] != undefined){
            $('#CompanyAddressStateId').val(data['state_id'])
            $('#CompanyAddressStateId').trigger("liszt:updated");
            gebo_chosen.updateChosenOptions('#CompanyAddressCityId', data['cities'], data['city_id'])
          }
        }
      }
    )
  })
  $('#CompanyAddressStateId').change(function(){
    $.getJSON($(this).data('cities-url'),
      {state_id: $(this).val()},
      function(data){
        gebo_chosen.updateChosenOptions('#CompanyAddressCityId',data);
      }
    )
  })

  $('#UserContactBirthday').datepicker({format: "dd/mm/yyyy", language: 'br'})
  $('#UserConfigurationInactiveAt').datepicker({format: "dd/mm/yyyy", language: 'br'})
  $('#UserProfessionalInformationAdmittedAt').datepicker({format: "dd/mm/yyyy", language: 'br'})
  $('#ProfessionalEventOccurAt').datepicker({format: "dd/mm/yyyy", language: 'br'})

  $('#addUserProfessionalEvent').click(function(e){
    e.preventDefault()
    if($('#ProfessionalEventId').val() == "" || $('#ProfessionalEventOccurAt').val() == ""){
      alert('Preencha Nome e Data do evento')
      return false
    }

    $("#table-user-professional-events tbody #zero-events").hide()
    var firstNewProfessionalEvent = $("#table-user-professional-events tbody .new-user-professional-event:first")
    var newId;
    if(firstNewProfessionalEvent.length == 0){
      newId = -1;
    }else{
      newId = parseInt(firstNewProfessionalEvent.data('pos'))-1;
    }

    var professionalEventProfessionalId = $('#ProfessionalEventId option:selected').val()
    var professionalEventOccurAt = $('#ProfessionalEventOccurAt').val().split('/').reverse().join('-')

    $("#table-user-professional-events tbody").prepend('<tr class="new-user-professional-event" data-pos="'+newId+'">'+
      '<td>'+$('#ProfessionalEventId option:selected').text()+
      '<input type="hidden" value="'+professionalEventProfessionalId+'" name="data[UserProfessionalEvent]['+newId+'][professional_event_id]" />'+
      '<input type="hidden" value="'+professionalEventOccurAt+'" name="data[UserProfessionalEvent]['+newId+'][occur_at]" />'+
      '</td>'+
      '<td>'+$('#ProfessionalEventOccurAt').val()+'</td>'+
      '<td class="text-center"><a class="btn btn-mini btn-danger">remover</a></td>'+
      '</tr>')
    $('#UserPartnerLastEventName').val($('#ProfessionalEventId option:selected').text())
    $('#UserPartnerLastEventOccurAt').val($('#ProfessionalEventOccurAt').val())

    $('#ProfessionalEventId').val('')
    $('#ProfessionalEventOccurAt').val('')
  })

  $('.new-user-professional-event a.btn-danger').live('click', function(){
    $(this).parent().parent().remove()
  })

  $('.user-professional-event a.btn-danger').live('click', function(){
    $(this).parent().parent().remove()
  })

  $('#UserAddressZipcode').change(function(){
    $.getJSON($(this).data('zipcode-url'),
      {zipcode: $(this).val()},
      function(data){
        if(data['success'] != 0){
          $('#UserAddressAddress').val(data['address'])
          if(data['state_id'] != undefined){
            $('#UserAddressStateId').val(data['state_id'])
            $('#UserAddressStateId').trigger("liszt:updated");
            gebo_chosen.updateChosenOptions('#UserAddressCityId', data['cities'], data['city_id'])
          }
        }
      }
    )
  })
  $('#UserAddressStateId').change(function(){
    $.getJSON($(this).data('cities-url'),
      {state_id: $(this).val()},
      function(data){
        gebo_chosen.updateChosenOptions('#UserAddressCityId',data);
      }
    )
  })

  $('#UserTemporaryOffStartAt').datepicker({format: "dd/mm/yyyy", language: 'br'})
  $('#UserTemporaryOffEndAt').datepicker({format: "dd/mm/yyyy", language: 'br'})

  toggleUserStatusFieldsDependencies()

  $("#UserConfigurationStatus").change(function(){
    toggleUserStatusFieldsDependencies()
  })

  $("#save-and-close-user-temporary-off").click(function(e){
    e.preventDefault()
    if(
      $("#UserTemporaryOffUserTemporaryOffMotiveId").val() == '' ||
      $("#UserTemporaryOffDocument").val() == '' ||
      $("#UserTemporaryOffStartAt").val() == '' ||
      $("#UserTemporaryOffEndAt").val() == ''
    ){
      alert('Preencha todos os campos da justificativa');
      return false;
    }

    $('#temporary-off').modal('hide')
  })
})

function toggleUserStatusFieldsDependencies(){
  hideUserTemporaryOffFields();
  hideUserInactiveFields()
  var status = $("#UserConfigurationStatus").val();
  if(status == "Inativo"){
    showUserInactiveFields()
  }else if(status == "Ausência Temporária"){
    showUserTemporaryOffFields()
  }
}

function showUserInactiveFields(){
  $('.inactiveUserWrapper').show();
  $('.inactiveUserWrapper select, inactiveUserWrapper input').attr('disabled', false);
}

function hideUserInactiveFields(){
  $('.inactiveUserWrapper').hide();
  $('.inactiveUserWrapper select, inactiveUserWrapper input').attr('disabled', 'disabled');
}

function showUserTemporaryOffFields(){
  $('.temporaryOffUserWrapper').show();
  $('#temporary-off input, #temporary-off select').attr('disabled', false);
}

function hideUserTemporaryOffFields(){
  $('.temporaryOffUserWrapper').hide();
  $('#temporary-off input, #temporary-off select').attr('disabled', 'disabled');
}
