<?php echo $this->Html->breadcrumb(array('<a href="#">Cadastros</a>', '<a href="#">Empresas</a>', $this->Html->link('Lista de Parceiros', array('action' => 'index')), 'Cadastro Parceiro')); ?>
<h3 class="heading">Empresas &raquo; Cadastro Parceiro</h3>

<div class="row-fluid">
	<div class="span12">
		<?php echo $this->BootstrapForm->create('Partner', array('class' => 'form-vertical well'));?>
			<?php echo $this->element('Forms/partner') ?>
			<?php echo $this->BootstrapForm->hidden('Partner.id'); ?>
			<?php echo $this->BootstrapForm->hidden('CompanyAddress.id'); ?>
			<?php echo $this->BootstrapForm->hidden('CompanyContact.id'); ?>
			<?php echo $this->BootstrapForm->hidden('CompanyConfiguration.id'); ?>
		<?php echo $this->BootstrapForm->end();?>
	</div>
</div>
