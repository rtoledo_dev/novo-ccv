<?php
App::uses('AdminController', 'Controller');
/**
 * Regions Controller
 *
 * @property Region $Region
 */
class RegionsController extends AdminController {

	var $uses = array('Region','SearchRegion');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SearchRegion->recursive = 0;
		$this->SearchRegion->order = 'SearchRegion.name';
		$conditions = $this->_buildTableSearchConditions(array('SearchRegion.name LIKE',
			'Aov.name LIKE',
			'SearchRegion.user_employee_name LIKE'),
			@$this->request->query['q']);
		$this->set('searchRegions', $this->paginate('SearchRegion', $conditions));
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Region->id = $id;
		if (!$this->Region->exists()) {
			throw new NotFoundException(__('Invalid %s', __('region')));
		}
		$this->set('region', $this->Region->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Region->create();
			if ($this->Region->saveAll($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('region')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('region')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		
		$this->_buildFormAssociations();
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Region->id = $id;
		if (!$this->Region->exists()) {
			throw new NotFoundException(__('Invalid %s', __('region')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Region->saveAll($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('region')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('region')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Region->read(null, $id);
		}

		$this->_buildFormAssociations();
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Region->id = $id;
		if (!$this->Region->exists()) {
			throw new NotFoundException(__('Invalid %s', __('region')));
		}
		if ($this->Region->delete()) {
			$this->Session->setFlash(
				__('A informação foi removida com sucesso.', __('region')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(
			__('A informação não pode ser removida. Existe uma dependência da mesma no sistema.', __('region')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect($this->referer());
	}

	public function _buildFormAssociations()
	{
		$aovs = $this->Region->Aov->find('list', array('order' => 'name'));
		$this->set(compact('aovs'));
	}

	public function updateUserEmployee()
	{
		$userEmployee = false;
		if(is_numeric($this->request->query['region_id'])){
			$aov = $this->Region->Aov->findById($this->request->query['region_id']);
			$userEmployee['UserEmployee'] = $aov['UserEmployee'];
		}
		$this->set('userEmployee', $userEmployee);
		$this->render('/Elements/Region/userEmployee', 'ajax');
	}

}
