<?php echo $this->Html->breadcrumb(array('<a href="#">Cadastros</a>', '<a href="#">Usuários</a>', 'Lista de Distribuidores')); ?>
<h3 class="heading"><?php echo __('Lista de %s', __('Distribuidores'));?></h3>

<div class="row-fluid">
	<div class="span4">
		<?php echo $this->Html->link('<i class="icon-plus-sign"></i> Cadastrar Novo',array('action' => 'add'), array('escape' => false, 'class' => 'btn')) ?>
	</div>
	<div class="span8">
		<?php echo $this->Form->create(NULL,array('class' => 'form-search pull-right', 'type' => 'get')) ?>
		<input type="text" placeholder="Buscar" name="q" value="<?php echo @$this->request->query['q'] ?>" />
		<?php echo $this->Form->end(NULL) ?>
	</div>
</div>

<div class="row-fluid search-table-row">
	<div class="span12">

		<table class="table table-striped table-bordered table-condensed">
			<thead>
			<tr>
				<th><?php echo $this->BootstrapPaginator->sort('name',__('Nome'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('UserContact.birthday',__('Data de Nascimento'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('UserContact.cpf',__('CPF'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('Company.name',__('Distribuidor'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('ProfessionalPosition.name',__('Cargo'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('ProfessionalOccupation.name',__('Atividade Primária'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('Role.name', __('Perfil'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('UserConfiguration.see_group',__('Visualização do Grupo'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('UserConfiguration.status', __('Status'));?></th>
				<th class="td-options"></th>
			</tr>
			</thead>
			<tbody>
		<?php foreach ($userDistributors as $userDistributor): ?>
			<tr>
				<td><?php echo h($userDistributor['SearchUserDistributor']['name']); ?>&nbsp;</td>
				<td><?php echo $this->Time->format($userDistributor['UserContact']['birthday'], '%d/%m/%Y'); ?>&nbsp;</td>
				<td><?php echo h($userDistributor['UserContact']['cpf']); ?>&nbsp;</td>
				<td><?php echo h($userDistributor['Company']['name']); ?>&nbsp;</td>
				<td><?php echo h($userDistributor['ProfessionalPosition']['name']); ?>&nbsp;</td>
				<td><?php echo h($userDistributor['ProfessionalOccupation']['name']); ?>&nbsp;</td>
				<td><?php echo h($userDistributor['Role']['name']); ?>&nbsp;</td>
				<td><?php echo h($userDistributor['UserConfiguration']['see_group'] ? 'Sim' : 'Não'); ?>&nbsp;</td>
				<td><?php echo h($userDistributor['UserConfiguration']['status']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link('<i class="icon-edit"></i>', array('action' => 'edit', $userDistributor['SearchUserDistributor']['id']), array('class' => 'left-td-options', 'escape' => false, 'title' => __('Editar'))); ?>
					<?php echo $this->Form->postLink('<i class="icon-remove-sign align-icon"></i>', array('action' => 'delete', $userDistributor['SearchUserDistributor']['id']), array('class'=> 'right-td-options', 'escape' => false, 'title' => __('Excluir')), __('Deseja prosseguir com esta ação?')); ?>
				</td>
			</tr>
		<?php endforeach; ?>
			</tbody>
		</table>
		<span class="pull-right">
			<?php echo $this->BootstrapPaginator->pagination(); ?>
		</span>
	</div>
</div>
