<?php
App::uses('AppModel', 'Model');
/**
 * SearchAdministrator Model
 *
 * @property Company $Company
 * @property State $State
 * @property City $City
 * @property Role $Role
 */
class SearchAdministrator extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'State' => array(
			'className' => 'State',
			'foreignKey' => 'state_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'City' => array(
			'className' => 'City',
			'foreignKey' => 'city_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Role' => array(
			'className' => 'Role',
			'foreignKey' => 'role_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);


	/**
 * hasOne associations
 *
 * @var array
 */
  public $hasOne = array(
    'UserContact' => array(
      'className' => 'UserContact',
      'foreignKey' => 'user_id',
    ),
    'UserConfiguration' => array(
      'className' => 'UserConfiguration',
      'foreignKey' => 'user_id',
    ),
    'UserAddress' => array(
      'className' => 'UserAddress',
      'foreignKey' => 'user_id',
    ),
  );
}
