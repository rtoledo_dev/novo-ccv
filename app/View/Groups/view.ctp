<div class="row-fluid">
	<div class="span9">
		<h2><?php  echo __('Group');?></h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($group['Group']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Name'); ?></dt>
			<dd>
				<?php echo h($group['Group']['name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Created At'); ?></dt>
			<dd>
				<?php echo h($group['Group']['created_at']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Updated At'); ?></dt>
			<dd>
				<?php echo h($group['Group']['updated_at']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Edit %s', __('Group')), array('action' => 'edit', $group['Group']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete %s', __('Group')), array('action' => 'delete', $group['Group']['id']), null, __('Are you sure you want to delete # %s?', $group['Group']['id'])); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Groups')), array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Group')), array('action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Company Configurations')), array('controller' => 'company_configurations', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Company Configuration')), array('controller' => 'company_configurations', 'action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Group Companies')), array('controller' => 'group_companies', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Group Company')), array('controller' => 'group_companies', 'action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Group Manufacturers')), array('controller' => 'group_manufacturers', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Group Manufacturer')), array('controller' => 'group_manufacturers', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span9">
		<h3><?php echo __('Related %s', __('Company Configurations')); ?></h3>
	<?php if (!empty($group['CompanyConfiguration'])):?>
		<table class="table">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Company Id'); ?></th>
				<th><?php echo __('Aov Id'); ?></th>
				<th><?php echo __('Region Id'); ?></th>
				<th><?php echo __('Group Id'); ?></th>
				<th><?php echo __('Created At'); ?></th>
				<th><?php echo __('Updated At'); ?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($group['CompanyConfiguration'] as $companyConfiguration): ?>
			<tr>
				<td><?php echo $companyConfiguration['id'];?></td>
				<td><?php echo $companyConfiguration['company_id'];?></td>
				<td><?php echo $companyConfiguration['aov_id'];?></td>
				<td><?php echo $companyConfiguration['region_id'];?></td>
				<td><?php echo $companyConfiguration['group_id'];?></td>
				<td><?php echo $companyConfiguration['created_at'];?></td>
				<td><?php echo $companyConfiguration['updated_at'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'company_configurations', 'action' => 'view', $companyConfiguration['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'company_configurations', 'action' => 'edit', $companyConfiguration['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'company_configurations', 'action' => 'delete', $companyConfiguration['id']), null, __('Are you sure you want to delete # %s?', $companyConfiguration['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>

	</div>
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('Company Configuration')), array('controller' => 'company_configurations', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="row-fluid">
	<div class="span9">
		<h3><?php echo __('Related %s', __('Group Companies')); ?></h3>
	<?php if (!empty($group['GroupCompany'])):?>
		<table class="table">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Group Id'); ?></th>
				<th><?php echo __('Company Id'); ?></th>
				<th><?php echo __('Created At'); ?></th>
				<th><?php echo __('Updated At'); ?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($group['GroupCompany'] as $groupCompany): ?>
			<tr>
				<td><?php echo $groupCompany['id'];?></td>
				<td><?php echo $groupCompany['group_id'];?></td>
				<td><?php echo $groupCompany['company_id'];?></td>
				<td><?php echo $groupCompany['created_at'];?></td>
				<td><?php echo $groupCompany['updated_at'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'group_companies', 'action' => 'view', $groupCompany['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'group_companies', 'action' => 'edit', $groupCompany['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'group_companies', 'action' => 'delete', $groupCompany['id']), null, __('Are you sure you want to delete # %s?', $groupCompany['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>

	</div>
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('Group Company')), array('controller' => 'group_companies', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="row-fluid">
	<div class="span9">
		<h3><?php echo __('Related %s', __('Group Manufacturers')); ?></h3>
	<?php if (!empty($group['GroupManufacturer'])):?>
		<table class="table">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Group Id'); ?></th>
				<th><?php echo __('Manufacturer Id'); ?></th>
				<th><?php echo __('Created At'); ?></th>
				<th><?php echo __('Updated At'); ?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($group['GroupManufacturer'] as $groupManufacturer): ?>
			<tr>
				<td><?php echo $groupManufacturer['id'];?></td>
				<td><?php echo $groupManufacturer['group_id'];?></td>
				<td><?php echo $groupManufacturer['manufacturer_id'];?></td>
				<td><?php echo $groupManufacturer['created_at'];?></td>
				<td><?php echo $groupManufacturer['updated_at'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'group_manufacturers', 'action' => 'view', $groupManufacturer['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'group_manufacturers', 'action' => 'edit', $groupManufacturer['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'group_manufacturers', 'action' => 'delete', $groupManufacturer['id']), null, __('Are you sure you want to delete # %s?', $groupManufacturer['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>

	</div>
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('Group Manufacturer')), array('controller' => 'group_manufacturers', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
