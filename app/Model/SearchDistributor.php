<?php
App::uses('AppModel', 'Model');
/**
 * Distributor Model
 *
 * @property Manufacturer $Manufacturer
 */
class SearchDistributor extends AppModel {

/**
 * Display field
 *
 * @var string
 */
  public $displayField = 'name';


  //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
  public $belongsTo = array(
    'Manufacturer' => array(
      'className' => 'Manufacturer',
      'foreignKey' => 'manufacturer_id',
      'conditions' => '',
      'fields' => '',
      'order' => ''
    ),
    'CompanyRating' => array(
      'className' => 'CompanyRating',
      'foreignKey' => 'company_rating_id',
      'conditions' => '',
      'fields' => '',
      'order' => ''
    ),
    'CertificationTsw' => array(
      'className' => 'CertificationTsw',
      'foreignKey' => 'certification_tsw_id',
      'conditions' => '',
      'fields' => '',
      'order' => ''
    ),
    'Region' => array(
      'className' => 'Region',
      'foreignKey' => 'region_id',
      'conditions' => '',
      'fields' => '',
      'order' => ''
    ),
    'Group' => array(
      'className' => 'Group',
      'foreignKey' => 'group_id',
      'conditions' => '',
      'fields' => '',
      'order' => ''
    ),
    'Aov' => array(
      'className' => 'Aov',
      'foreignKey' => 'aov_id',
      'conditions' => '',
      'fields' => '',
      'order' => ''
    ),
  );

/**
 * hasOne associations
 *
 * @var array
 */
  public $hasOne = array(
    'DistributorInformation' => array(
      'className' => 'DistributorInformation',
      'foreignKey' => 'company_id',
    ),
    'CompanyContact' => array(
      'className' => 'CompanyContact',
      'foreignKey' => 'company_id',
    ),
    'CompanyConfiguration' => array(
      'className' => 'CompanyConfiguration',
      'foreignKey' => 'company_id',
    ),
    'CompanyAddress' => array(
      'className' => 'CompanyAddress',
      'foreignKey' => 'company_id',
    ),
  );
}
