<label class="label-1">Buscar por:</label>
<input class="input-1" name="search[term]" type="text" value="<?php echo @$search['term'] ?>" onBlur="if(this.value=='') this.value=''" onFocus="if(this.value =='' ) this.value=''"  />
<label class="label-1">Valor inicial:</label>
<input class="input-1" name="search[start_value]" type="text" value="<?php echo @$search['start_value'] ?>" onBlur="if(this.value=='') this.value=''" onFocus="if(this.value =='' ) this.value=''"  />
<label class="label-2">Valor final:</label>
<input class="input-1" name="search[end_value]" type="text" value="<?php echo @$search['end_value'] ?>" onBlur="if(this.value=='') this.value=''" onFocus="if(this.value =='' ) this.value=''"  />
<label class="label-2">Tipo de imóvel:</label>
<div class="select-2">
  <select name="search[type]">
    <option value="">&nbsp;</option>
    <option value="Aluguel" <?php echo $this->Search->isSelected($search,'type','Aluguel') ?>>Aluguel</option>
    <option value="Venda" <?php echo $this->Search->isSelected($search,'type','Venda') ?>>Venda</option>
    <option value="Projeto" <?php echo $this->Search->isSelected($search,'type','Projeto') ?>>Projeto</option>
    <option value="Loteamento" <?php echo $this->Search->isSelected($search,'type','Loteamento') ?>>Loteamento</option>
  </select>
</div>
<label class="label-2">Cidade:</label>
<div class="select-2">
  <select name="search[city]">
    <option value="">&nbsp;</option>
    <?php foreach ($allCities as $city): ?>
      <option value="<?php echo $city ?>" <?php echo $this->Search->isSelected($search,'city',$city) ?>><?php echo $city ?></option>
    <?php endforeach ?>
  </select></div>
<label class="label-2">Quartos:</label>
<div class="select-2"><select name="search[rooms]">
    <option value="">&nbsp;</option>
  <?php for ($i=1; $i < 10; $i++): ?>
    <option value="<?php echo $i ?>" <?php echo $this->Search->isSelected($search,'rooms',$i) ?>><?php echo $i ?></option>
  <?php endfor;?>
  </select>
</div>
<div class="clear"></div>