<?php
App::uses('AppModel', 'Model');
/**
 * UserInactiveReason Model
 *
 */
class UserInactiveReason extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
