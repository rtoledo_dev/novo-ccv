<?php
App::uses('AppModel', 'Model');
/**
 * EducationFormation Model
 *
 * @property EducationFormationGroup $EducationFormationGroup
 */
class EducationFormation extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'EducationFormationGroup' => array(
			'className' => 'EducationFormationGroup',
			'foreignKey' => 'education_formation_group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
