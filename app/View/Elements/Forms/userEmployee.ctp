<fieldset>
  <legend class="block-title"><?php echo __('Dados Pessoais'); ?></legend>
  <?php
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    ?>
    <label for="UserEmployeephoto" class="control-label">Foto</label>
    <div class="controls">
      <?php echo $this->SimpleForm->imageUpload('UserEmployee', 'photo', '100x100') ?>
    </div>
    <?
    echo '</div>';
    echo '<div class="span8">';
    echo $this->BootstrapForm->input('name', array('label' => 'Nome Completo', 'size' => 50));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('UserContact.sex', array(
        'label' => array('text' => 'Sexo', 'class' => 'inline'),
        'options' => array('Masculino' => 'Masculino', 'Feminino' => 'Feminino'),
        'type' => 'radio'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserContact.birthday', array('label' => 'Data de Nascimento', 'type' => 'text',
      'value' => $this->SimpleForm->formatDateFromModel(@$this->request->data['UserContact']['birthday'])));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserContact.rg', array('label' => 'RG'));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserContact.cpf', array('label' => 'CPF', 'class' => 'mask-cpf'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('company_id', array('label' => 'Filial',
      'options' => $branchs,
      'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Dados de Contato'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('email', array('label' => 'Email'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('confirm_email', array('label' => 'Confirmação do Email'));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('UserContact.mobile', array('label' => 'Telefone Celular',
        'class' => 'mask-phone',
        'size' => 10));
    echo '</div>';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('UserContact.phone_comercial', array('label' => 'Telefone Comercial',
        'class' => 'mask-phone',
        'size' => 10));
    echo '</div>';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('UserContact.phone_house', array('label' => 'Telefone Residencial',
        'class' => 'mask-phone',
        'size' => 10));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Endereço'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserAddress.zipcode', array('label' => 'CEP',
      'class' => 'mask-zipcode',
      'data-zipcode-url' => $this->Html->url(array('action' => 'addressByZipcode'))));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserAddress.state_id', array('label' => 'Estado',
      'class' => 'chzn_a',
      'data-cities-url' => $this->Html->url(array('action' => 'citiesByState')),
      'empty' => true));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserAddress.city_id', array('label' => 'Cidade', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserAddress.address', array('label' => 'Endereço'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserAddress.address_complement', array('label' => 'Complemento', 'size' => 20));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Vestimenta'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserGarb.tshirt_id', array('label' => 'Camiseta', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserGarb.shoe_id', array('label' => 'Calçado', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Escolaridade'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserEducationInformation.literacy', array('label' => 'Grau de Instrução', 'options' => $this->SimpleForm->usersLiteraciesOptions(), 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
    echo '<div class="span6">';
    echo $this->BootstrapForm->input('EducationFormation.EducationFormation', array(
      'label' => 'Formação', 'options' => $this->SimpleForm->usersEducationFormationsOptions($educationFormationGroups),
      'class' => 'multiselect', 'selected' => $this->SimpleForm->multiSelectAssociationIds('EducationFormation.EducationFormation'))
    );
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Dados Profissionais'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserProfessionalInformation.admitted_at', array('label' => 'Data de Admissão', 'type' => 'text',
      'value' => $this->SimpleForm->formatDateFromModel(@$this->request->data['UserProfessionalInformation']['admitted_at'])));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserProfessionalInformation.professional_position_id', array('label' => 'Cargo', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('ProfessionalDepartment.ProfessionalDepartment', array(
      'label' => 'Departamento', 'options' => $professionalDepartments, 'multiple' => 'multiple',
      'class' => 'multiselect', 'selected' => $this->SimpleForm->multiSelectAssociationIds('ProfessionalDepartment.ProfessionalDepartment'))
    );
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserProfessionalInformation.formations', array('label' => 'Formações', 'type' => 'textarea', 'rows' => 12, 'class' => 'span12'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserProfessionalInformation.duties', array('label' => 'Principais Atribuições', 'type' => 'textarea', 'rows' => 12, 'class' => 'span12'));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Experiência Anterior'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('Manufacturer.Manufacturer', array(
      'label' => 'Experiência em outras Marcas', 'multiple' => 'multiple',
      'class' => 'multiselect', 'selected' => $this->SimpleForm->multiSelectAssociationIds('Manufacturer.Manufacturer'))
    );
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('ProfessionalOtherDepartment.ProfessionalOtherDepartment', array(
      'label' => 'Experiência em outros Setores', 'multiple' => 'multiple',
      'options' => $professionalDepartments,
      'class' => 'multiselect', 'selected' => $this->SimpleForm->multiSelectAssociationIds('ProfessionalOtherDepartment.ProfessionalOtherDepartment'))
    );
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserLastJob.company', array('label' => 'Nome da Empresa'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserLastJob.department', array('label' => 'Departamento'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserLastJob.duration', array('label' => 'Duração'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserLastJob.professional_position', array('label' => 'Cargo', 'type' => 'text'));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Configurações'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('UserConfiguration.role_id', array('label' => 'Perfil', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('UserConfiguration.status', array('label' => 'Status', 'options' => $this->SimpleForm->administratorsStatusOptions()));
    echo '</div>';
    echo '<div class="span4 inactiveUserWrapper">';
      echo $this->BootstrapForm->input('UserConfiguration.user_inactive_reason_id', array('label' => 'Razão', 'empty' => true));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span8">';
    echo '</div>';
    echo '<div class="span4 inactiveUserWrapper">';
      echo $this->BootstrapForm->input('UserConfiguration.user_inactive_motive_id', array('label' => 'Motivo', 'empty' => true));
    echo '</div>';
  echo '</div>';
  ?>
  <?php echo $this->BootstrapForm->submit(__('Salvar'));?>
</fieldset>
