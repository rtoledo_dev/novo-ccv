<?php echo $this->Html->breadcrumb(array('<a href="#">Cadastros</a>', $this->Html->link('Lista de Companys', array('action' => 'index')), 'Cadastro Company')); ?>
<h3 class="heading">Cadastro Company</h3>

<div class="row-fluid">
	<div class="span12">
		<?php echo $this->BootstrapForm->create('Company', array('class' => 'form-horizontal well'));?>
			<?php echo $this->element('Forms/Company') ?>
			<fieldset>
				<legend class="block-title"><?php echo __('Dados %s', __('Company')); ?></legend>
				<?php
				echo '<div class="row-fluid">';
					echo '<div class="span6">';
					echo $this->BootstrapForm->input('manufacturer_id');
					echo '</div>';
				echo '</div>';
				echo '<div class="row-fluid">';
					echo '<div class="span6">';
					echo $this->BootstrapForm->input('name');
					echo '</div>';
				echo '</div>';
				echo '<div class="row-fluid">';
					echo '<div class="span6">';
					echo $this->BootstrapForm->input('fiscal_name');
					echo '</div>';
				echo '</div>';
				echo '<div class="row-fluid">';
					echo '<div class="span6">';
					echo $this->BootstrapForm->input('cnpj');
					echo '</div>';
				echo '</div>';
				echo '<div class="row-fluid">';
					echo '<div class="span6">';
					echo $this->BootstrapForm->input('domain');
					echo '</div>';
				echo '</div>';
				echo '<div class="row-fluid">';
					echo '<div class="span6">';
					echo $this->BootstrapForm->input('state_reference');
					echo '</div>';
				echo '</div>';
				echo '<div class="row-fluid">';
					echo '<div class="span6">';
					echo $this->BootstrapForm->input('company_type');
					echo '</div>';
				echo '</div>';
				echo '<div class="row-fluid">';
					echo '<div class="span6">';
					echo $this->BootstrapForm->input('created_at');
					echo '</div>';
				echo '</div>';
				echo '<div class="row-fluid">';
					echo '<div class="span6">';
					echo $this->BootstrapForm->input('updated_at');
					echo '</div>';
				echo '</div>';
				echo $this->BootstrapForm->hidden('id');
				?>
				<?php echo $this->BootstrapForm->submit(__('Salvar'));?>
			</fieldset>
		<?php echo $this->BootstrapForm->end();?>
	</div>
</div>
