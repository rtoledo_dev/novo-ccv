<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
	Router::connect('/', array('controller' => 'Home', 'action' => 'index'));
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
  Router::connect('/imoveis/alugar', array('controller' => 'Imoveis', 'action' => 'index', 'search' => array('type' => 'Aluguel')));
  Router::connect('/imoveis/comprar', array('controller' => 'Imoveis', 'action' => 'index', 'search' => array('type' => 'Venda')));
  Router::connect('/imoveis/projetos', array('controller' => 'Imoveis', 'action' => 'index', 'search' => array('type' => 'Projeto')));
  Router::connect('/imoveis/loteamentos', array('controller' => 'Imoveis', 'action' => 'index', 'search' => array('type' => 'Loteamento')));
  
  Router::connect('/sobre-empresa', array('controller' => 'Paginas', 'action' => 'view'));
  Router::connect('/fortguel', array('controller' => 'Paginas', 'action' => 'view'));
  Router::connect('/arn-engenharia', array('controller' => 'Paginas', 'action' => 'view'));
  Router::connect('/vender-alugar-imoveis', array('controller' => 'Paginas', 'action' => 'view'));

  Router::connect('/fale-conosco', array('controller' => 'Contact', 'action' => 'view'));


/**
 * Load all plugin routes.  See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
