<?php echo $this->Html->breadcrumb(array('<a href="#">Cadastros</a>', '<a href="#">Usuários</a>', 'Lista de Administradores')); ?>
<h3 class="heading"><?php echo __('Lista de %s', __('Administradores'));?></h3>

<div class="row-fluid">
	<div class="span4">
		<?php echo $this->Html->link('<i class="icon-plus-sign"></i> Cadastrar Novo',array('action' => 'add'), array('escape' => false, 'class' => 'btn')) ?>
	</div>
	<div class="span8">
		<?php echo $this->Form->create(NULL,array('class' => 'form-search pull-right', 'type' => 'get')) ?>
		<input type="text" placeholder="Buscar" name="q" value="<?php echo @$this->request->query['q'] ?>" />
		<?php echo $this->Form->end(NULL) ?>
	</div>
</div>

<div class="row-fluid search-table-row">
	<div class="span12">

		<table class="table table-striped table-bordered table-condensed">
			<thead>
			<tr>
				<th><?php echo $this->BootstrapPaginator->sort('name',__('Nome'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('UserContact.birthday',__('Data de Nascimento'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('UserContact.cpf',__('CPF'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('UserContact.phone_comercial',__('Telefone Comercial'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('UserContact.mobile',__('Telefone Celular'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('City.name',__('Cidade'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('State.short_name',__('UF'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('Role.name', __('Perfil'));?></th>
				<th><?php echo $this->BootstrapPaginator->sort('UserConfiguration.status', __('Status'));?></th>
				<th class="td-options"></th>
			</tr>
			</thead>
			<tbody>
		<?php foreach ($administrators as $administrator): ?>
			<tr>
				<td><?php echo h($administrator['SearchAdministrator']['name']); ?>&nbsp;</td>
				<td><?php echo $this->Time->format($administrator['UserContact']['birthday'], '%d/%m/%Y'); ?>&nbsp;</td>
				<td><?php echo h($administrator['UserContact']['cpf']); ?>&nbsp;</td>
				<td><?php echo h($administrator['UserContact']['phone_comercial']); ?>&nbsp;</td>
				<td><?php echo h($administrator['UserContact']['mobile']); ?>&nbsp;</td>
				<td><?php echo h($administrator['City']['name']); ?>&nbsp;</td>
				<td><?php echo h($administrator['State']['short_name']); ?>&nbsp;</td>
				<td><?php echo h($administrator['Role']['name']); ?>&nbsp;</td>
				<td><?php echo h($administrator['UserConfiguration']['status']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link('<i class="icon-edit"></i>', array('action' => 'edit', $administrator['SearchAdministrator']['id']), array('class' => 'left-td-options', 'escape' => false, 'title' => __('Editar'))); ?>
					<?php echo $this->Form->postLink('<i class="icon-remove-sign align-icon"></i>', array('action' => 'delete', $administrator['SearchAdministrator']['id']), array('class'=> 'right-td-options', 'escape' => false, 'title' => __('Excluir')), __('Deseja prosseguir com esta ação?')); ?>
				</td>
			</tr>
		<?php endforeach; ?>
			</tbody>
		</table>
		<span class="pull-right">
			<?php echo $this->BootstrapPaginator->pagination(); ?>
		</span>
	</div>
</div>
