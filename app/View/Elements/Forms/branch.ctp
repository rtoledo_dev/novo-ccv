<fieldset>
  <legend class="block-title"><?php echo __('Dados Cadastrais'); ?></legend>
  <?php
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('name', array('label' => 'Nome'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('fiscal_name', array('label' => 'Razão Social', 'size' => 80));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('cnpj', array('label' => 'CNPJ', 'class' => 'mask-cnpj'));
    echo '</div>';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('state_reference', array('label' => 'Inscrição Estadual', 'size' => 20));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Endereço'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyAddress.zipcode', array('label' => 'CEP',
      'class' => 'mask-zipcode',
      'data-zipcode-url' => $this->Html->url(array('action' => 'addressByZipcode'))));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyAddress.state_id', array('label' => 'UF',
      'class' => 'chzn_a',
      'data-cities-url' => $this->Html->url(array('action' => 'citiesByState')),
      'empty' => true));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyAddress.city_id', array('label' => 'Cidade', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyAddress.address', array('label' => 'Endereço'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyAddress.address_complement', array('label' => 'Complemento', 'size' => 20));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyContact.phone_comercial1', array('label' => 'Telefone', 'class' => 'mask-phone', 'size' => 10));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Configurações'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('CompanyConfiguration.status', array('label' => 'Status', 'options' => $this->SimpleForm->distributorsStatusOptions()));
    echo '</div>';
  echo '</div>';
  ?>
  <?php echo $this->BootstrapForm->submit(__('Salvar'));?>
</fieldset>
