<?php echo $this->Html->breadcrumb(array('<a href="#">Cadastros</a>', '<a href="#">Usuários</a>', $this->Html->link('Lista de Funcionários', array('action' => 'index')), 'Cadastro Funcionário')); ?>
<h3 class="heading">Usuários &raquo; Cadastro Funcionário</h3>

<div class="row-fluid">
	<div class="span12">
		<?php echo $this->BootstrapForm->create('UserEmployee', array('class' => 'form-vertical well', 'type' => 'file'));?>
			<?php echo $this->element('Forms/userEmployee') ?>
		<?php echo $this->BootstrapForm->end();?>
	</div>
</div>
