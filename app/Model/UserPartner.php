<?php
App::uses('AppModel', 'Model');
/**
 * UserPartner Model
 *
 * @property Company $Company
 */
class UserPartner extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
	'company_id' => array(
		'numeric' => array(
			'rule' => array('numeric'),
			'message' => '* Deve ser preenchido',
			//'allowEmpty' => false,
			// 'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
		),
	),
	'name' => array(
		'notempty' => array(
			'rule' => array('notempty'),
			'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	'email' => array(
		'notempty' => array(
			'rule' => array('notempty'),
			'message' => '* Deve ser preenchido',
              //'allowEmpty' => false,
              //'required' => false,
              //'last' => false, // Stop validation after this rule
              //'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		'email' => array(
			'rule' => array('email'),
			'message' => 'Deve ser um email válido.',
              //'allowEmpty' => false,
              //'required' => false,
              //'last' => false, // Stop validation after this rule
              //'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		'isUnique' => array(
			'rule' => array('isUnique'),
			'message' => 'Já existe um registro com esse valor',
			'allowEmpty' => true,
			),
		),
	'confirm_email' => array(
		'equalToEmail' => array(
			'rule' => array('equalToEmail'),
			'message' => '* Email não é igual',
              //'allowEmpty' => false,
			'required' => false,
              //'last' => false, // Stop validation after this rule
			'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	// 'pass' => array(
	// 	'notempty' => array(
	// 		'rule' => array('notempty'),
	// 		'message' => '* Deve ser preenchido',
	// 		'allowEmpty' => false,
 //              //'required' => false,
 //              //'last' => false, // Stop validation after this rule
 //              'on' => 'create', // Limit validation to 'create' or 'update' operations
 //              ),
	// 	'minLength' => array(
	// 		'rule' => array('minLength',3),
	// 		'message' => 'Mínimo de 3 caracteres.',
	// 		'allowEmpty' => true,
 //              //'required' => false,
 //              //'last' => false, // Stop validation after this rule
 //              // 'on' => 'create', // Limit validation to 'create' or 'update' operations
	// 		),
	// 	),
	// 'confirm_password' => array(
	// 	'equalToPassword' => array(
	// 		'rule' => array('equalToPassword'),
	// 		'message' => 'Não confere com a senha.',
 //              //'allowEmpty' => false,
	// 		'required' => false,
 //              //'last' => false, // Stop validation after this rule
 //              // 'on' => 'create', // Limit validation to 'create' or 'update' operations
	// 		),
	// 	),
	// 'password' => array(
	// 	'notempty' => array(
	// 		'rule' => array('notempty'),
	// 		'message' => '* Deve ser preenchido',
	// 			//'allowEmpty' => false,
	// 			//'required' => false,
	// 			//'last' => false, // Stop validation after this rule
	// 			//'on' => 'create', // Limit validation to 'create' or 'update' operations
	// 		),
	// 	),
	'user_type' => array(
		'notempty' => array(
			'rule' => array('notempty'),
			'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
public $belongsTo = array(
	'Partner' => array(
		'className' => 'Partner',
		'foreignKey' => 'company_id',
		'conditions' => '',
		'fields' => '',
		'order' => ''
		)
	);

/**
 * hasOne associations
 *
 * @var array
 */
public $hasOne = array(
	'UserContact' => array(
		'className' => 'UserContact',
		'foreignKey' => 'user_id',
		),
	'UserConfiguration' => array(
		'className' => 'UserConfiguration',
		'foreignKey' => 'user_id',
		),
	'UserAddress' => array(
		'className' => 'UserAddress',
		'foreignKey' => 'user_id',
		),
	'UserGarb' => array(
		'className' => 'UserGarb',
		'foreignKey' => 'user_id',
		),
	'UserEducationInformation' => array(
		'className' => 'UserEducationInformation',
		'foreignKey' => 'user_id',
		),
	'UserProfessionalInformation' => array(
		'className' => 'UserProfessionalInformation',
		'foreignKey' => 'user_id',
		),
	'UserManufacturerExperiencie' => array(
		'className' => 'UserManufacturerExperiencie',
		'foreignKey' => 'user_id',
		),
	'UserLastJob' => array(
		'className' => 'UserLastJob',
		'foreignKey' => 'user_id',
		),
	
	);

var $hasMany = array(
	'UserProfessionalEvent' => array(
		'className' => 'UserProfessionalEvent',
		'foreignKey' => 'user_id',
		'order' => 'UserProfessionalEvent.occur_at DESC'
		),
);

var $hasAndBelongsToMany = array(
	'EducationFormation' => array(
		'className' => 'EducationFormation',
		'joinTable' => 'user_education_formations',
		'foreignKey' => 'user_id',
		'associationForeignKey' => 'education_formation_id',
		'with' => 'UserEducationFormation',
		'unique' => 'keepExisting',
		),
	'Manufacturer' => array(
		'className' => 'Manufacturer',
		'joinTable' => 'user_manufacturer_experiencies',
		'foreignKey' => 'user_id',
		'associationForeignKey' => 'manufacturer_id',
		'with' => 'UserManufacturerExperiency',
		'unique' => 'keepExisting',
		),
	'ProfessionalDepartment' => array(
		'className' => 'ProfessionalDepartment',
		'joinTable' => 'user_other_professional_departaments',
		'foreignKey' => 'user_id',
		'associationForeignKey' => 'professional_department_id',
		'with' => 'UserOtherProfessionalDepartament',
		'unique' => 'keepExisting',
		),
	);


	public $actsAs = array(
	'Upload.Upload' => array(
		'photo' => array(
			'thumbnailSizes' => array(
				'640x480' => '640x480',
				'100x100' => '100x100'
				),
			'thumbnailQuality' => 90
			),
		)
	);


	public function beforeSave($options)
	{
		$userProfessionalEventsIdsToKeep = array(0);
		if(count(@$this->data['UserProfessionalEvent'])){

			foreach ($this->data['UserProfessionalEvent'] as $key => $userProfessionalEvent) {
				if(array_key_exists('id', $userProfessionalEvent['UserProfessionalEvent'])){
					$userProfessionalEventExists = $this->UserProfessionalEvent->find('first',
						array(
							'conditions' => array(
								'UserProfessionalEvent.id' => $userProfessionalEvent['UserProfessionalEvent']['id'],
								'UserProfessionalEvent.user_id' => $this->id
							)
						)
					);
					if($userProfessionalEventExists){
						$userProfessionalEventsIdsToKeep[] = $userProfessionalEvent['UserProfessionalEvent']['id'];
					}
				}
			}
		}
		$this->UserProfessionalEvent->deleteAll('UserProfessionalEvent.user_id = '.$this->id.' AND UserProfessionalEvent.id NOT IN('.join(',',$userProfessionalEventsIdsToKeep).')');
		return true;
	}
}
