<?php
App::uses('AdminController', 'Controller');
/**
 * Companies Controller
 *
 * @property Branch $Branch
 */
class BranchsController extends AdminController {

	var $uses = array('Branch','SearchBranch');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SearchBranch->recursive = 0;
		$this->SearchBranch->order = 'SearchBranch.name';
		$conditions = $this->_buildTableSearchConditions(array(
			'SearchBranch.name LIKE',
			'SearchBranch.cnpj LIKE',
			'State.name LIKE',
			'City.name LIKE',
			'CompanyConfiguration.status LIKE',
			'SearchBranch.fiscal_name LIKE'),@$this->request->query['q']);
		$this->set('branchs', $this->paginate('SearchBranch', $conditions));
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Branch->id = $id;
		if (!$this->Branch->exists()) {
			throw new NotFoundException(__('Invalid %s', __('branch')));
		}
		$this->set('branch', $this->Branch->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Branch->create();
			if ($this->Branch->saveAll($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('branch')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('branch')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$this->_buildFormAssociations();
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Branch->id = $id;
		if (!$this->Branch->exists()) {
			throw new NotFoundException(__('Invalid %s', __('branch')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Branch->saveAll($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('branch')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('branch')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Branch->read(null, $id);
		}
		$this->_buildFormAssociations();
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Branch->id = $id;
		if (!$this->Branch->exists()) {
			throw new NotFoundException(__('Invalid %s', __('branch')));
		}
		if ($this->Branch->delete()) {
			$this->Session->setFlash(
				__('A informação foi removida com sucesso.', __('branch')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(
			__('A informação não pode ser removida. Existe uma dependência da mesma no sistema.', __('branch')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect($this->referer());
	}

	public function _buildFormAssociations()
	{
		$states = $this->Branch->CompanyAddress->State->find('list');
		$cities = array();
		if(!empty($this->request->data['CompanyAddress']['state_id'])){
			$cities = $this->Branch->CompanyAddress->City->find('list', array('conditions' => array(
				'City.state_id' => $this->request->data['CompanyAddress']['state_id']
				)
			));
		}

		$this->set(compact('states', 'cities'));
	}

}
