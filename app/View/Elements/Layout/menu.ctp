<ul class="nav" id="mobile-nav">
  <li class="dropdown">
    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon-list-alt icon-white"></i> Cadastros <b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li><?php echo $this->Html->link('AOV', array('controller' => 'Aovs')) ?></li>
      <li><?php echo $this->Html->link('Grupo', array('controller' => 'Groups')) ?></li>
      <li><?php echo $this->Html->link('Região', array('controller' => 'Regions')) ?></li>
      <li><a href="_lista_locais_treinamento.php">Locais de Treinamento</a></li>
      <li><a href="_lista_trilhas.php">Trilhas</a></li>
      <li><a href="_lista_turmas.php">Turmas</a></li>
      <li><a href="_lista_cargos_trilhas.php">Cargos x Trilhas</a></li>
      <li><a href="_lista_treinamentos.php">Treinamentos</a></li>
      <li><a href="_cadastro_pesquisas.php">Pesquisas</a></li>

      <li>
        <a href="#">Empresas <b class="caret-right"></b></a>
        <ul class="dropdown-menu">
          <li><?php echo $this->Html->link('Distribuidor', array('controller' => 'Distributors')) ?></li>
          <li><?php echo $this->Html->link('Filial', array('controller' => 'Branchs')) ?></li>
          <li><?php echo $this->Html->link('Parceiro', array('controller' => 'Partners')) ?></li>
        </ul>
      </li>

      <li>
        <a href="#">Usuários <b class="caret-right"></b></a>
        <ul class="dropdown-menu">
          <li><?php echo $this->Html->link('Administrador', array('controller' => 'Administrators')) ?></li>
          <li><?php echo $this->Html->link('Distribuidor', array('controller' => 'UserDistributors')) ?></li>
          <li><?php echo $this->Html->link('Funcionário', array('controller' => 'UserEmployees')) ?></li>
          <li><?php echo $this->Html->link('Parceiro', array('controller' => 'UserPartners')) ?></li>
        </ul>
      </li>
      <li>
        <a href="#">Conteúdo <b class="caret-right"></b></a>
        <ul class="dropdown-menu">
          <li><a href="_lista_conteudo_banners.php">Banners</a></li>
          <li><a href="_cadastro_conteudo_imglogin.php">Imagens do Login</a></li>
          <li><a href="_lista_conteudo_noticia.php">Notícias</a></li>
          <li><a href="_lista_conteudo_videos.php">Videos</a></li>
          <li><a href="_lista_conteudo_tv_toyota.php">TV Toyota</a></li>
          <li><a href="_lista_conteudo_toyota_sales_news.php">Toyota Sales News</a></li>
          <li><a href="_lista_conteudo_biblioteca.php">Biblioteca</a></li>
          <li><a href="_lista_conteudo_produto.php">Produto</a></li>
          <li><a href="_lista_conteudo_paginas_institucionais.php">Páginas Institucionais</a></li>
        </ul>
      </li>
    </ul>
  </li>
</ul>
