<fieldset>
  <legend class="block-title"><?php echo __('Dados Pessoais'); ?></legend>
  <?php
  echo '<div class="row-fluid">';
  echo '<div class="span4">';
    ?>
    <label for="UserPartnerphoto" class="control-label">Foto</label>
    <div class="controls">
      <?php echo $this->SimpleForm->imageUpload('UserPartner', 'photo', '100x100') ?>
    </div>
    <?
    echo '</div>';
    echo '<div class="span8">';
    echo $this->BootstrapForm->input('name', array('label' => 'Nome Completo', 'size' => 50));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('UserContact.sex', array(
        'label' => array('text' => 'Sexo', 'class' => 'inline'),
        'options' => array('Masculino' => 'Masculino', 'Feminino' => 'Feminino'),
        'type' => 'radio'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserContact.birthday', array('label' => 'Data de Nascimento', 'type' => 'text',
      'value' => $this->SimpleForm->formatDateFromModel(@$this->request->data['UserContact']['birthday'])));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserContact.rg', array('label' => 'RG'));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('UserContact.cpf', array('label' => 'CPF', 'class' => 'mask-cpf'));
    echo '</div>';
    echo '<div class="span4">';
    echo $this->BootstrapForm->input('company_id', array('label' => 'Empresa',
      'options' => $partners,
      'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Dados de Contato'); ?></legend>
  <?
  echo '<div class="row-fluid">';
  echo '<div class="span4">';
  echo $this->BootstrapForm->input('email', array('label' => 'Email'));
  echo '</div>';
  echo '<div class="span4">';
  echo $this->BootstrapForm->input('confirm_email', array('label' => 'Confirmação do Email'));
  echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('UserContact.mobile', array('label' => 'Telefone Celular',
        'class' => 'mask-phone',
        'size' => 10));
    echo '</div>';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('UserContact.phone_comercial', array('label' => 'Telefone Comercial',
        'class' => 'mask-phone',
        'size' => 10));
    echo '</div>';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('UserContact.phone_house', array('label' => 'Telefone Residencial',
        'class' => 'mask-phone',
        'size' => 10));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Endereço'); ?></legend>
  <?
  echo '<div class="row-fluid">';
  echo '<div class="span4">';
  echo $this->BootstrapForm->input('UserAddress.zipcode', array('label' => 'CEP',
    'class' => 'mask-zipcode',
    'data-zipcode-url' => $this->Html->url(array('action' => 'addressByZipcode'))));
  echo '</div>';
  echo '<div class="span4">';
  echo $this->BootstrapForm->input('UserAddress.state_id', array('label' => 'Estado',
    'class' => 'chzn_a',
    'data-cities-url' => $this->Html->url(array('action' => 'citiesByState')),
    'empty' => true));
  echo '</div>';
  echo '<div class="span4">';
  echo $this->BootstrapForm->input('UserAddress.city_id', array('label' => 'Cidade', 'class' => 'chzn_a', 'empty' => true));
  echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
  echo '<div class="span4">';
  echo $this->BootstrapForm->input('UserAddress.address', array('label' => 'Endereço'));
  echo '</div>';
  echo '<div class="span4">';
  echo $this->BootstrapForm->input('UserAddress.address_complement', array('label' => 'Complemento', 'size' => 20));
  echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Vestimenta'); ?></legend>
  <?
  echo '<div class="row-fluid">';
  echo '<div class="span4">';
  echo $this->BootstrapForm->input('UserGarb.tshirt_id', array('label' => 'Camiseta', 'class' => 'chzn_a', 'empty' => true));
  echo '</div>';
  echo '<div class="span4">';
  echo $this->BootstrapForm->input('UserGarb.shoe_id', array('label' => 'Calçado', 'class' => 'chzn_a', 'empty' => true));
  echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Escolaridade'); ?></legend>
  <?
  echo '<div class="row-fluid">';
  echo '<div class="span4">';
  echo $this->BootstrapForm->input('UserEducationInformation.literacy', array('label' => 'Grau de Instrução', 'options' => $this->SimpleForm->usersLiteraciesOptions(), 'class' => 'chzn_a', 'empty' => true));
  echo '</div>';
  echo '<div class="span6">';
  echo $this->BootstrapForm->input('EducationFormation.EducationFormation', array(
    'label' => 'Formação', 'options' => $this->SimpleForm->usersEducationFormationsOptions($educationFormationGroups),
    'class' => 'multiselect', 'selected' => $this->SimpleForm->multiSelectAssociationIds('EducationFormation.EducationFormation'))
  );
  echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Dados Profissionais'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('UserProfessionalInformation.professional_position_id', array('label' => 'Cargo',
        'options' => $professionalPositions,
        'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('last_event_name', array('label' => 'Último Evento',
        'disabled' => 'disabled',
        'value' => @$lastUserProfessionalEvent['ProfessionalEvent']['name']));
    echo '</div>';
    echo '<div class="span4">';
      echo '<label>&nbsp;</label>';
      echo '<a data-toggle="modal" class="btn" href="#user-professional-events" aria-describedby="ui-tooltip-0"><i class="icon-plus-sign"></i> Adicionar Evento</a>';
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo '</div>';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('last_event_occur_at', array('label' => 'Data do Último Evento', 'type' => 'text',
        'disabled' => 'disabled',
        'value' => $this->SimpleForm->formatDateFromModel(@$lastUserProfessionalEvent['occur_at'])));
    echo '</div>';
  echo '</div>';
  ?>
  <legend class="block-title"><?php echo __('Configurações'); ?></legend>
  <?
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('UserConfiguration.role_id', array('label' => 'Perfil', 'class' => 'chzn_a', 'empty' => true));
    echo '</div>';
    echo '<div class="span4">';
      echo $this->BootstrapForm->input('UserConfiguration.status', array('label' => 'Status', 'options' => $this->SimpleForm->administratorsStatusOptions()));
    echo '</div>';
    echo '<div class="span4 inactiveUserWrapper">';
      echo $this->BootstrapForm->input('UserConfiguration.inactive_at', array('label' => 'Data de Inativação', 'type' => 'text',
        'value' => $this->SimpleForm->formatDateFromModel(@$this->request->data['UserConfiguration']['inactive_at'])));
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span4">';
    echo '</div>';
    echo '<div class="span4 inactiveUserWrapper">';
      echo $this->BootstrapForm->input('UserConfiguration.user_inactive_reason_id', array('label' => 'Razão', 'empty' => true));
    echo '</div>';
    echo '<div class="span4 inactiveUserWrapper">';
      echo $this->BootstrapForm->input('UserConfiguration.user_inactive_motive_id', array('label' => 'Motivo', 'empty' => true));
    echo '</div>';
  echo '</div>';
  ?>
  <?php echo $this->BootstrapForm->submit(__('Salvar'));?>
</fieldset>

<div class="modal hide fade" id="user-professional-events">
  <div class="modal-header">
    <button class="close" data-dismiss="modal">×</button>
    <h3>Eventos</h3>
  </div>
  <div class="modal-body">

    <legend class="block-title">Inserir Evento</legend>
    <div class="row-fluid">

      <div class="span4">
        <label>Nome do evento</label>
        <select id="ProfessionalEventId" id="ProfessionalEventId" class="span12">
          <option value="">Selecione</option>
          <?php foreach ($professionalEvents as $professionalEventId => $professionalEventName): ?>
            <option value="<?php echo $professionalEventId ?>"><?php echo $professionalEventName ?></option>
          <?php endforeach ?>
        </select>
      </div>
      <div class="span3">
        <label>Data do evento</label>
        <input type="text" class="span8" name="ProfessionalEventOccurAt" id="ProfessionalEventOccurAt" />
      </div>
      <div class="span3">
        <label>&nbsp;</label>
        <a href="javascript:;" id="addUserProfessionalEvent" class="btn">Cadastrar</a>
      </div>
    </div>

    <legend class="block-title">Histórico de Eventos Cadastrados</legend>

    <div class="row-fluid">
      <div class="span12">
        <table class="table table-striped table-bordered table-condensed" id="table-user-professional-events">
          <thead>
            <tr>
              <th>Nome do Evento</th>
              <th>Data</th>
              <th width="80"></th>
            </tr>
          </thead>
          <tbody>
            <?php if (count(@$this->request->data['UserProfessionalEvent'])): ?>
              <?php foreach ($this->request->data['UserProfessionalEvent'] as $iProfessionalEvent => $userProfessionalEvent): ?>
              <tr class="<?php echo (empty($userProfessionalEvent['id']) ? 'new-user-professional-event' : 'user-professional-event') ?>"
                  data-pos="<?php echo $iProfessionalEvent ?>"
                  data-user-professional-event-id="<?php echo @$userProfessionalEvent['id'] ?>">
                <td>
                  <?php echo $userProfessionalEvent['ProfessionalEvent']['name'] ?>
                  <?php if (!empty($userProfessionalEvent['id'])): ?>
                    <?php echo $this->Form->hidden('UserProfessionalEvent.'.$iProfessionalEvent.'.id') ?>
                  <?php endif ?>
                  <?php echo $this->Form->hidden('UserProfessionalEvent.'.$iProfessionalEvent.'.professional_event_id') ?>
                  <?php echo $this->Form->hidden('UserProfessionalEvent.'.$iProfessionalEvent.'.occur_at') ?>
                </td>
                <td><?php echo $this->SimpleForm->formatDateFromModel($userProfessionalEvent['occur_at']) ?></td>
                <td class="text-center"><a class="ttip_r btn btn-mini btn-danger" title="Importante: para concretizar a remoção desta informação é necessário salvar o formulário">remover</a></td>
              </tr>
              <?php endforeach ?>
            <?php else: ?>
              <tr id="zero-events">
                <td colspan="3">Nenhum evento encontrado</td>
              </tr>
            <?php endif ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
