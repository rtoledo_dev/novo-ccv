<?php
App::uses('AdminController', 'Controller');
/**
 * Companies Controller
 *
 * @property Distributor $Distributor
 */
class DistributorsController extends AdminController {
	var $uses = array('Distributor','SearchDistributor');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SearchDistributor->recursive = 0;
		$this->SearchDistributor->order = 'SearchDistributor.name';
		$conditions = $this->_buildTableSearchConditions(array('SearchDistributor.name LIKE',
			'DistributorInformation.dlr_toyos LIKE',
			'Manufacturer.name LIKE',
			'DistributorInformation.initialized_at',
			'CompanyContact.email LIKE',
			'CompanyContact.phone_comercial1 LIKE',
			'CompanyRating.name LIKE',
			'CertificationTsw.name LIKE',
			'CompanyConfiguration.status LIKE'
			),@$this->request->query['q'], @$this->request->query['column']);
		$this->set('distributors', $this->paginate('SearchDistributor', $conditions));
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Distributor->id = $id;
		if (!$this->Distributor->exists()) {
			throw new NotFoundException(__('Invalid %s', __('distributor')));
		}
		$this->set('distributor', $this->Distributor->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Distributor->create();
			if ($this->Distributor->saveAll($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('distributor')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('distributor')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$this->_buildFormAssociations();
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Distributor->id = $id;
		if (!$this->Distributor->exists()) {
			throw new NotFoundException(__('Invalid %s', __('distributor')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Distributor->saveAll($this->request->data)) {
				$this->Session->setFlash(
					__('As informações foram guardadas com sucesso!', __('distributor')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('Não foi possível salvar. Verifique os campos preenchidos e tente novamente.', __('distributor')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Distributor->read(null, $id);
		}
		$this->_buildFormAssociations();
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Distributor->id = $id;
		if (!$this->Distributor->exists()) {
			throw new NotFoundException(__('Invalid %s', __('distributor')));
		}
		if ($this->Distributor->delete()) {
			$this->Session->setFlash(
				__('A informação foi removida com sucesso.', __('distributor')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(
			__('A informação não pode ser removida. Existe uma dependência da mesma no sistema.', __('distributor')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect($this->referer());
	}

	public function _buildFormAssociations()
	{
		$states = $this->Distributor->CompanyAddress->State->find('list');
		$cities = array();
		if(!empty($this->request->data['CompanyAddress']['state_id'])){
			$cities = $this->Distributor->CompanyAddress->City->find('list', array('conditions' => array(
				'City.state_id' => $this->request->data['CompanyAddress']['state_id']
				)
			));
		}
		
		$certificationTsws = $this->Distributor->CompanyConfiguration->CertificationTsw->find('list');
		$companyRatings = $this->Distributor->CompanyConfiguration->CompanyRating->find('list');
		$groups = $this->Distributor->CompanyConfiguration->Group->find('list');
		$regions = $this->Distributor->CompanyConfiguration->Region->find('list');
		$manufacturers = $this->Distributor->Manufacturer->find('list');
		$this->set(compact('manufacturers','regions','groups','certificationTsws','companyRatings', 'states', 'cities'));
	}

	public function updateCompanyConfigurationAov()
	{
		$companyConfigurationAov = false;
		if(is_numeric($this->request->query['region_id'])){
			$region = $this->Distributor->CompanyConfiguration->Region->findById($this->request->query['region_id']);
			$companyConfigurationAov['Aov'] = $region['Aov'];
		}
		$this->set('companyConfigurationAov', $companyConfigurationAov);
		$this->render('/Elements/Distributor/companyConfigurationAov', 'ajax');
	}

}
