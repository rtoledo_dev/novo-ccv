<?php
App::uses('AppModel', 'Model');
/**
 * SearchUserPartner Model
 *
 * @property Company $Company
 * @property Role $Role
 */
class SearchUserPartner extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Company' => array(
			'className' => 'Company',
			'foreignKey' => 'company_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Role' => array(
			'className' => 'Role',
			'foreignKey' => 'role_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ProfessionalPosition' => array(
      'className' => 'ProfessionalPosition',
      'foreignKey' => 'professional_position_id',
    ),
	);


	/**
 * hasOne associations
 *
 * @var array
 */
  public $hasOne = array(
    'UserContact' => array(
      'className' => 'UserContact',
      'foreignKey' => 'user_id',
    ),
    'UserConfiguration' => array(
      'className' => 'UserConfiguration',
      'foreignKey' => 'user_id',
    ),
    'UserAddress' => array(
      'className' => 'UserAddress',
      'foreignKey' => 'user_id',
    ),
  );
}
