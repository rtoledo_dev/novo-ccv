<?
class LayoutHelper extends AppHelper{
  public function menuName($menu, $currentMenu){
    if($menu == $currentMenu){
      return '<u>'.$menu.'</u>';
    }else{
      return $menu;
    }
  }

  public function filePath($className, $field, $size = '140x140')
  {
    $path = Inflector::underscore($className);
    $id = @$this->request->data[$className]['id'];
    $filename = @$this->request->data[$className][$field];
    if($size){
      $filename = $size.'_'.$filename;
    }
    $path = 'files/'.$path.'/'.$field.'/'.$id.'/'.$filename;
    if(file_exists($path)){
      return '/'.$path;
    }else{
      return '/images/missing.png';
    }
  }

  public function imagePath($id,$filename,$size = '80x60')
  {
    if($size){
      $filename = $size.'_'.$filename;
    }
    $path = 'files/real_estate_image/photo/'.$id.'/'.$filename;
    if(file_exists($path)){
      return '/'.$path;
    }else{
      return '/images/real_estate_blank.gif';
    }
  }
}
