<?php
App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class DashboardHelper extends Helper {

  public function weeks($currentWeek = false)
  {
    if(!$currentWeek){
      $currentWeek = date('Y-m-d', strtotime('Last Sunday', time()));
    }

    $currentWeekInTime = strtotime($currentWeek);


    $weeks = array();
    for ($i=-3; $i <= 3; $i++) {
      $daysToMath    = $i*7;
      $beginOfWeek   = strtotime("$i week",$currentWeekInTime);
      $endOfWeek     = strtotime("Next Saturday",$beginOfWeek);
      $inCurrentWeek = $beginOfWeek <= $currentWeekInTime && $endOfWeek >= $currentWeekInTime;
      $weeks[]       = array(
        'inCurrentWeek' => $inCurrentWeek,
        'beginOfWeek'   => date('Y-m-d',$beginOfWeek), 
        'text'          => 'Semana '.date('d/m',$beginOfWeek).' a '.date('d/m',$endOfWeek)
      );
    }
    return $weeks;
  }
}