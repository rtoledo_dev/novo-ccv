<fieldset>
  <legend class="block-title"><?php echo __('Dados %s', __('Grupo')); ?></legend>
  <?php
  echo '<div class="row-fluid">';
    echo '<div class="span12">';
    echo $this->BootstrapForm->input('name', array(
      'label' => 'Nome')
    );
    echo '</div>';
  echo '</div>';
  echo '<div class="row-fluid">';
    echo '<div class="span6">';
    echo $this->BootstrapForm->input('Distributor.Distributor', array(
      'label' => 'Distribuidores Relacionados', 'multiple'=> 'multiple',
      'options' => $distributors,
      'selected' => $this->SimpleForm->multiSelectAssociationIds('Distributor.Distributor'))
    );
    echo '</div>';
    echo '<div class="span6">';
    echo $this->BootstrapForm->input('Manufacturer.Manufacturer', array(
      'label' => 'Outras Marcas', 'class' => 'multiselect', 'multiple'=> 'multiple',
      'options' => $manufacturers,
      'selected' => $this->SimpleForm->multiSelectAssociationIds('Manufacturer.Manufacturer'))
    );
    echo '</div>';
  echo '</div>';
  ?>
  </div>
  <?php echo $this->BootstrapForm->submit(__('Salvar'));?>
</fieldset>


<div id="distributorsDetails"></div>
<?php $this->start('footerScripts') ?>
<script type="text/javascript">
$(function(){
  updateDistributorsDetails();
  $("select#DistributorDistributor").multiSelect({
    selectableHeader  : '<h4>Disponíveis</h4>',
    selectedHeader    : '<h4>Selecionados</h4>',
    afterSelect: function(values){
      updateDistributorsDetails()
    },
    afterDeselect: function(values){
      updateDistributorsDetails()
    }
  });
})
function updateDistributorsDetails(){
  var ids = $("select#DistributorDistributor").val();
  $.get('<?php echo $this->Html->url(array('action' => 'updateDistributorsDetails')) ?>',
    {distributorsIds: ids},
    function(data){
      $('#distributorsDetails').html(data);
    }
  )
}
</script>
<?php $this->end() ?>
