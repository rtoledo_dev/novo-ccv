<?php echo $this->Html->breadcrumb(array(
    $this->Html->link('Lista de los profesionales',array('action' => 'index')),
    'Cambio de clave',
)); ?>

<?php echo $this->BootstrapForm->create('UserChangePass', array('class' => 'form-horizontal'));?>
	<h3 class="heading">Cambio de Clave</h3>

  <fieldset>
    <div class="row-fluid">
      <div class="span6">
      <?
      echo $this->BootstrapForm->input('current_pass', array(
        'class' => 'span7', 'value'=>'', 'autocomplete'=>'off', 'type' => 'password', 'label' => 'Contraseña atual')
      );
      ?>
      </div>
    </div>
    <div class="row-fluid">
      <div class="span6">
      <?
      echo $this->BootstrapForm->input('pass', array(
        'class' => 'span7', 'label' => 'Nueva contraseña', 'value'=>'', 'autocomplete'=>'off', 'type' => 'password')
      );
      ?>
      </div>
      <div class="span6">
      <?
      echo $this->BootstrapForm->input('confirm_password', array(
        'class' => 'span7', 'label' => 'Confirma la nueva contraseña', 'value'=>'', 'autocomplete'=>'off', 'type' => 'password')
      );
      ?>
      </div>
    </div>
    
    <div class="form-actions">
      <button type="submit" class="btn btn-primary">Cambiar</button>
    </div>
  </fieldset>
<?php echo $this->BootstrapForm->end();?>