<?php
App::uses('AppModel', 'Model');
/**
 * UserContact Model
 *
 * @property User $User
 */
class UserContact extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'sex' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'birthday' => array(
			'date' => array(
				'rule' => array('date'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'rg' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cpf' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => '* Deve ser preenchido',
				'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'cpf' => array(
				'rule' => array('cpf', true),
				'message' => '* Deve ser um CPF válido'
			),
		),
		'phone_comercial' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'phone_house' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'mobile' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => '* Deve ser preenchido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);


	public function beforeValidate($options = array()) {
    $this->dateFormatBeforeSave($this->data['UserContact']['birthday']);
 	  return true;
	}
}
