<?php echo $this->Html->breadcrumb(array('<a href="#">Cadastros</a>', $this->Html->link('Lista de AOVs', array('action' => 'index')), 'Cadastro AOV')); ?>

<h3 class="heading"><!-- Cadastros &raquo;  -->Cadastro AOV</h3>


<div class="row-fluid">
	<div class="span12">
		<?php echo $this->BootstrapForm->create('Aov', array('class' => 'form-verical well'));?>
			<?php echo $this->element('Forms/aov') ?>
		<?php echo $this->BootstrapForm->end();?>
	</div>
</div>
